package com.anantoni.qpo.catalog;

public enum TypeEnum {
    INT, VARCHAR, CHAR, FLOAT, DECIMAL, DOUBLE
}
