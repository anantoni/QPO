package com.anantoni.qpo.catalog;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.exception.UnsanitizableException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

public class Key implements Serializable {
    List<Integer> domains;
    List<Attribute> __domains;
    IndexEnum indexType;
    IndexAttributes indexAttributes;

    protected List<Integer> getUnsanitizedDomains() {
        return domains;
    }

    public List<Attribute> getDomains() {
        return __domains;
    }

    public IndexEnum getIndexType() {
        return indexType;
    }

    public IndexAttributes getIndexAttributes() {
        return indexAttributes;
    }

    protected void sanitize(List<Attribute> attrList) throws UnsanitizableException {
        __domains = new ArrayList<>();

        for (Integer domId : domains) {
            Attribute attr = attrList.get(domId);

            if (attr == null) {
                throw new UnsanitizableException("Attribute " + domId + " not found");
            }

            __domains.add(attr);
        }
    }
}
