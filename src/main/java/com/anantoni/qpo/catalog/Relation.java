package com.anantoni.qpo.catalog;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.exception.UnsanitizableException;

import java.io.Serializable;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

public class Relation implements Serializable {
    private String relationName;
    private String fileName;
    private List<Attribute> attributeList;
    private Key primaryKey;
    private List<Key> secondaryKeyList;
    private List<KeyReference> foreignKeyList;
    private Integer cardinality;
    private Integer tupleSize;
    private Integer tupleAmount;

    public Relation() {
    }

    /**
     * Builds relation and domain references from the JSON Integer data in Key
     * and KeyReference objects
     */
    void sanitize(List<Relation> relationList) throws UnsanitizableException {
        getPrimaryKey().sanitize(this.attributeList);

        for (Key secKey : getSecondaryKeyList()) {
            secKey.sanitize(this.attributeList);
        }

        for (KeyReference krf : getForeignKeyList()) {
            krf.sanitize(relationList, this.attributeList);
        }

        for (Attribute attr : this.attributeList) {
            attr.setRelation(this);
        }
    }

    public boolean hasAttribute(String attrName) {
        for (Attribute attr : getAttributeList()) {
            if (attr.name.equals(attrName)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return the relationName
     */
    public String getRelationName() {
        return relationName;
    }

    /**
     * @param relationName the relationName to set
     */
    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return the attributeList
     */
    public List<Attribute> getAttributeList() {
        return attributeList;
    }

    /**
     * @param attributeList the attributeList to set
     */
    public void setAttributeList(List<Attribute> attributeList) {
        this.attributeList = attributeList;
    }

    /**
     * @return the primaryKey
     */
    public Key getPrimaryKey() {
        return primaryKey;
    }

    /**
     * @param primaryKey the primaryKey to set
     */
    public void setPrimaryKey(Key primaryKey) {
        this.primaryKey = primaryKey;
    }

    /**
     * @return the secondaryKeyList
     */
    public List<Key> getSecondaryKeyList() {
        return secondaryKeyList;
    }

    /**
     * @param secondaryKeyList the secondaryKeyList to set
     */
    public void setSecondaryKeyList(List<Key> secondaryKeyList) {
        this.secondaryKeyList = secondaryKeyList;
    }

    /**
     * @return the foreignKeyList
     */
    public List<KeyReference> getForeignKeyList() {
        return foreignKeyList;
    }

    /**
     * @param foreignKeyList the foreignKeyList to set
     */
    public void setForeignKeyList(List<KeyReference> foreignKeyList) {
        this.foreignKeyList = foreignKeyList;
    }

    /**
     * @return the cardinality
     */
    public Integer getCardinality() {
        return cardinality;
    }

    /**
     * @param cardinality the cardinality to set
     */
    public void setCardinality(Integer cardinality) {
        this.cardinality = cardinality;
    }

    /**
     * @return the tupleSize
     */
    public Integer getTupleSize() {
        return tupleSize;
    }

    /**
     * @param tupleSize the tupleSize to set
     */
    public void setTupleSize(Integer tupleSize) {
        this.tupleSize = tupleSize;
    }

    public Integer getTupleAmount() {
        return tupleAmount;
    }

    public void setTupleAmount(Integer tupleAmount) {
        this.tupleAmount = tupleAmount;
    }
}
