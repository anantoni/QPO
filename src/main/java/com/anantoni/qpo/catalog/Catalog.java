package com.anantoni.qpo.catalog;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.exception.UnsanitizableException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

//~--- JDK imports ------------------------------------------------------------

public class Catalog {
    Integer memoryAmountOfBuffers;
    Integer memoryBufferSizeFrames;
    Integer memoryFrameSizeBytes;
    Integer diskAccessLatencyMillisecs;
    Integer diskTransferTimeMillisecs;
    Integer diskPageSizeBytes;
    List<Relation> relationList;
    Map<String, Relation> relationMap;

    public Catalog() {
        relationMap = new HashMap<>();
    }

    public Integer getMemoryAmountOfBuffers() {
        return memoryAmountOfBuffers;
    }

    public Integer getMemoryBufferSizeFrames() {
        return memoryBufferSizeFrames;
    }

    public Integer getMemoryFrameSizeBytes() {
        return memoryFrameSizeBytes;
    }

    public Integer getDiskAccessLatencyMillisecs() {
        return diskAccessLatencyMillisecs;
    }

    public Integer getDiskTransferTimeMillisecs() {
        return diskTransferTimeMillisecs;
    }

    public Integer getDiskPageSizeBytes() {
        return diskPageSizeBytes;
    }

    public List<Relation> getRelationList() {
        return relationList;
    }

    public void sanitize() throws UnsanitizableException {
        for (Relation r : relationList) {
            r.sanitize(relationList);
            relationMap.put(r.getRelationName(), r);
        }
    }

    public Relation getRelationFromName(String relationName) {
        Relation r = relationMap.get(relationName);

        if (r == null) {
            System.err.println("No relation " + relationName + " in database.");
            System.exit(1);
        }

        return r;
    }
}
