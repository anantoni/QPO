package com.anantoni.qpo.catalog.exception;

public class UnsanitizableException extends Exception {
    public UnsanitizableException(String message) {
        super(message);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
