package com.anantoni.qpo.catalog;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.exception.UnsanitizableException;

import java.util.ArrayList;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

public class KeyReference {
    List<Integer> domains;
    String foreignRelation;
    private Relation __foreignRelation;
    private List<Attribute> __domains;

    protected List<Integer> getUnsanitizedDomains() {
        return domains;
    }

    public List<Attribute> getDomains() {
        return __domains;
    }

    public Relation getForeignRelation() {
        return __foreignRelation;
    }

    protected void setForeignRelation(Relation foreignRelation) {
        __foreignRelation = foreignRelation;
    }

    protected String getUnsanitizedForeignRelation() {
        return foreignRelation;
    }

    void sanitize(List<Relation> relationList, List<Attribute> attrList) throws UnsanitizableException {
        __foreignRelation = null;

        for (Relation r : relationList) {
            if (r.getRelationName().equals(foreignRelation)) {
                __foreignRelation = r;

                break;
            }
        }

        if (__foreignRelation == null) {
            throw new UnsanitizableException("Foreign Relation not found for name \"" + foreignRelation + "\"");
        }

        __domains = new ArrayList<>();

        for (Integer domId : domains) {
            Attribute attr = attrList.get(domId);

            if (attr == null) {
                throw new UnsanitizableException("Attribute " + domId + " not found");
            }

            __domains.add(attr);
        }
    }
}
