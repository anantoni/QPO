package com.anantoni.qpo.catalog;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

public class IndexAttributes implements Serializable {
    IndexEnum type;
    Integer height;
    Integer fanOut;
    Integer loadFactor;
    Integer discreteKeyValueAmount;

    public IndexEnum getType() {
        return type;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getFanOut() {
        return fanOut;
    }

    public Integer getLoadFactor() {
        return loadFactor;
    }

    public Integer getDiscreteKeyValueAmount() {
        return discreteKeyValueAmount;
    }
}
