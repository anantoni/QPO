package com.anantoni.qpo.catalog;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Attribute implements Serializable {
    String name;
    String type;
    Integer size;
    Integer uniqueValues;
    Integer minValue;
    Integer maxValue;
    boolean allValuesUnique;
    Map<Integer, Integer> histogram;
    private Relation relation;

    public Attribute(String name, String type, Integer size,
                     Integer uniqueValues, Integer minValue, Integer maxValue, Map<Integer, Integer> histogram) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.uniqueValues = uniqueValues;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.histogram = histogram;
    }

    public Attribute(Attribute attr) {
        name = attr.name;
        type = attr.type;
        size = attr.size;
        relation = attr.relation;
        uniqueValues = attr.uniqueValues;
        minValue = attr.minValue;
        maxValue = attr.maxValue;
        if (attr.histogram != null) {
            histogram = new HashMap<>();
            histogram.putAll(attr.histogram);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if ((obj == null) || !obj.getClass().equals(Attribute.class)) {
            return false;
        }

        Attribute other = (Attribute) obj;
        boolean retVal = true;

        retVal = retVal && ((name == null)
                ? (other.name == null)
                : name.equals(other.name));
        retVal = retVal && ((type == null)
                ? (other.type == null)
                : type.equals(other.type));
        retVal = retVal && ((name == null)
                ? (other.size == null)
                : size.equals(other.size));

        return retVal;
    }

    @Override
    public int hashCode() {
        int hash = 7;

        hash = 11 * hash + Objects.hashCode(this.name);
        hash = 11 * hash + Objects.hashCode(this.type);
        hash = 11 * hash + Objects.hashCode(this.size);

        return hash;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getUniqueValues() {
        return uniqueValues;
    }

    public void setUniqueValues(Integer uniqueValues) {
        this.uniqueValues = uniqueValues;
    }

    public Integer getMinValue() {
        return minValue;
    }

    public void setMinValue(Integer minValue) {
        this.minValue = minValue;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Integer maxValue) {
        this.maxValue = maxValue;
    }

    public Relation getRelation() {
        return relation;
    }

    public void setRelation(Relation relation) {
        this.relation = relation;
    }

    public void removeRelationPrefix() {
        name = name.replace(".*\\.", "");
    }

    @Override
    public String toString() {
        return name + " " + type;
    }
}
