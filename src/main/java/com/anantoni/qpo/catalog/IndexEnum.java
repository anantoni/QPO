package com.anantoni.qpo.catalog;

/**
 * @author efthymiosh
 */
public enum IndexEnum {
    BPlusTree, Hash, ISAM
}
