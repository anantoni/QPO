package com.anantoni.qpo.optimizer;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.query_tree.TreeNode;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

//~--- JDK imports ------------------------------------------------------------

public class TreePrinter {
    PrintWriter pw;
    int graphID = 1;

    public TreePrinter() {
        try {
            pw = new PrintWriter("graph.dot", "UTF-8");
            pw.println("strict digraph query_graph {");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            pw = null;
        }
    }

    public void printTree(TreeNode root, String graphTitle) {
        if (pw == null)
            return;
        pw.println("subgraph cluster_" + graphID + "{\ngraph [label=\"" + graphTitle + "\"];");
        root.printDotNotation(pw, graphID++);
        pw.println("}");
    }

    public void presentGraph() {
        if (pw != null) {
            pw.println("}");
            pw.close();

            String[] dotcmdarray = {"dot", "graph.dot", "-Tpng", "-o", "graph.png"};
            String[] xdgcmdarray = {"xdg-open", "graph.png"};

            try {
                Runtime.getRuntime().exec(dotcmdarray).waitFor();
                Runtime.getRuntime().exec(xdgcmdarray);
            } catch (IOException | InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
