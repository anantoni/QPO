package com.anantoni.qpo.optimizer;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.exception.UnsanitizableException;
import com.anantoni.qpo.query_tree.*;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//~--- JDK imports ------------------------------------------------------------

public class Optimizer {
    private final TreeNode treeRoot;
    private final Catalog cat;

    public Optimizer(TreeNode root, Catalog cat) {
        treeRoot = root;
        this.cat = cat;
    }

    public static int nextIntegerlog(int base, int num) {
        return (int) Math.ceil(Math.log(num) / Math.log(base));
    }

    public void costEstimation() throws SyntaxErrorException {
        treeRoot.annotateSize();
        treeRoot.annotateCost(cat);
    }

    public void init() throws UnknownRelationException, UnsanitizableException {
        cat.sanitize();
        treeRoot.referenceBuildPass(cat, null);
    }

    public void pushSelections() {
        treeRoot.selectionSplit();

        List<TreeNode> parentList = new ArrayList<>();

        treeRoot.addListNodeParentsDFS(SelectionNode.class, parentList);

        ListIterator<TreeNode> li = parentList.listIterator(parentList.size());

        // Iterate in reverse order.
        while (li.hasPrevious()) {
            li.previous().selectionPushdownInit();
        }
    }

    public void optimizeJoins() {
        List<TreeNode> parentList = new ArrayList<>();

        /**
         * Find all JoinNode parents
         */
        treeRoot.addListNodeParentsDFS(JoinNode.class, parentList);

        for (int i = parentList.size() - 1; i > 0; i--) {

            List<JoinNode> joinNodeCluster = new ArrayList<>();
            parentList.get(i).getJoinNodeCluster(null, joinNodeCluster);
            Set<TreeNode> S = new LinkedHashSet<>();

            /**
             * Get the list of conditions for this join cluster
             */
            List<Condition> conditionList = null;
            JunctionEnum junctionType = null;
            for (JoinNode joinNode : joinNodeCluster) {
                conditionList = joinNode.getConditionList();
                junctionType = joinNode.getJunctionType();
                break;
            }

            /**
             * For each JoinNode in the cluster add its children TreeNodes
             * to S if they are not JoinNodes
             */
            for (JoinNode joinNode : joinNodeCluster) {
                if (!(joinNode.getOuterRelation() instanceof JoinNode)) {
                    S.add(joinNode.getOuterRelation());
                }

                if (!(joinNode.getInnerRelation() instanceof JoinNode)) {
                    S.add(joinNode.getInnerRelation());
                }
            }

            /**
             * Calculate the best join plan for the set S of TreeNodes
             */
            Map<Set<TreeNode>, BestPlan> bestPlanMap = new HashMap<>();
            BestPlan bestPlan = findBestPlan(S, bestPlanMap, conditionList, junctionType);

            parentList.get(i).setExpr(bestPlan.plan);

        }

        /**
         * Keep only parents that are not JoinNodes
         */
        ListIterator<TreeNode> li = parentList.listIterator();
        while (li.hasNext())
            if (li.next() instanceof JoinNode)
                li.remove();

        for (TreeNode joinParent : parentList) {
            List<JoinNode> joinNodeCluster = new ArrayList<>();

            joinParent.getJoinNodeCluster(null, joinNodeCluster);

            Set<TreeNode> S = new LinkedHashSet<>();

            /**
             * Get the list of conditions for this join cluster
             */
            List<Condition> conditionList = null;
            JunctionEnum junctionType = null;
            for (JoinNode joinNode : joinNodeCluster) {
                conditionList = joinNode.getConditionList();
                junctionType = joinNode.getJunctionType();
            }

            if (joinNodeCluster.size() == 1)
                continue;

            /**
             * For each JoinNode in the cluster add its children TreeNodes
             * to S if they are not JoinNodes
             */
            for (JoinNode joinNode : joinNodeCluster) {
                if (!(joinNode.getOuterRelation() instanceof JoinNode)) {
                    S.add(joinNode.getOuterRelation());
                }

                if (!(joinNode.getInnerRelation() instanceof JoinNode)) {
                    S.add(joinNode.getInnerRelation());
                }
            }

            /**
             * Calculate the best join plan for the set S of TreeNodes
             */
            Map<Set<TreeNode>, BestPlan> bestPlanMap = new HashMap<>();
            BestPlan bestPlan = findBestPlan(S, bestPlanMap, conditionList, junctionType);


            joinParent.setExpr(bestPlan.plan);

        }
    }

    public void optimizeMultiClusterJoins() {

        List<TreeNode> parentList = new ArrayList<>();

        /**
         * Find all JoinNode parents
         */
        treeRoot.addListNodeParentsDFS(JoinNode.class, parentList);
        /**
         * Keep only parents that are not JoinNodes
         */
        ListIterator<TreeNode> li = parentList.listIterator();
        while (li.hasNext())
            if (li.next() instanceof JoinNode)
                li.remove();

        for (TreeNode joinParent : parentList) {
            Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap = new LinkedHashMap<>();
            joinParent.fillMultiClusterMap(multiClusterMap);

            ListIterator<JoinClusterConditions> iter = new ArrayList<>(multiClusterMap.keySet()).listIterator(multiClusterMap.size());

            TreeNode nodeToPropagate = null;
            BestPlan bestPlan = null;
            while (iter.hasPrevious()) {
                JoinClusterConditions key = iter.previous();
                if (nodeToPropagate != null)
                    multiClusterMap.get(key).add(nodeToPropagate);

                /**
                 * Calculate the best join plan for the set S of TreeNodes
                 */
                Map<Set<TreeNode>, BestPlan> bestPlanMap = new HashMap<>();
                bestPlan = findBestPlan(multiClusterMap.get(key), bestPlanMap, key.getConditionList(), key.getJoinType());

                nodeToPropagate = bestPlan.plan;
            }

            joinParent.setExpr(bestPlan.plan);
        }
    }

    /**
     * @param S             the set of the leaf nodes of this join cluster
     * @param bestPlanMap   used for memoization
     * @param conditionList the condition on
     * @return the best join plan for this set of leaf nodes
     */
    public BestPlan findBestPlan(Set<TreeNode> S, Map<Set<TreeNode>, BestPlan> bestPlanMap, List<Condition> conditionList, JunctionEnum junctionType) {
        if (bestPlanMap.get(S) != null) {
            return bestPlanMap.get(S);
        }

        if (S.size() == 1) {
            Iterator<TreeNode> si = S.iterator();
            TreeNode plan = si.next();
            BestPlan bestPlan = new BestPlan();

            bestPlan.plan = plan;
            bestPlan.cost = plan.getCostRead();
            bestPlanMap.put(S, bestPlan);
        } else {
            List<Set<TreeNode>> subsetsList = subsets(S);

//            System.out.println("Number of subsets: " + subsetsList.size());

            for (Set<TreeNode> S1 : subsetsList) {
                BestPlan P1 = findBestPlan(S1, bestPlanMap, conditionList, junctionType);

                if (P1.plan == null) {
                    System.out.println("P1 is null");
                }

                Set<TreeNode> SminusS1 = new LinkedHashSet(S);

                SminusS1.removeAll(S1);

                BestPlan P2 = findBestPlan(SminusS1, bestPlanMap, conditionList, junctionType);

                if (P2.plan == null) {
                    System.out.println("P2 is null");
                }


                JoinNode joinNode = bestJoin(P1, P2, conditionList, junctionType);
                int cost = P1.cost + P2.cost + joinNode.getTotalCost();

                if ((bestPlanMap.get(S) == null) || (cost < bestPlanMap.get(S).cost)) {
                    BestPlan bestPlan = new BestPlan();

                    bestPlan.cost = cost;
                    bestPlan.plan = joinNode;
                    bestPlanMap.put(S, bestPlan);
                }
            }
        }

        if (bestPlanMap.get(S) == null) {
            System.out.println("Something is wrong, returning null");
        }

        return bestPlanMap.get(S);
    }

    /**
     * @param P1            BestPlan sub-tree
     * @param P2            BestPlan sub-tree
     * @param conditionList the list of conditions for this join cluster
     * @return the JoinNode with the optimal join cost
     */
    public JoinNode bestJoin(BestPlan P1, BestPlan P2, List<Condition> conditionList, JunctionEnum junctionType) {
        TreeNode toJoin1 = P1.plan;
        TreeNode toJoin2 = P2.plan;
        TreeNode outerRelation, innerRelation;

        if (toJoin1.getSizeBlocks() > toJoin2.getSizeBlocks()) {
            outerRelation = toJoin1;
            innerRelation = toJoin2;
        } else {
            outerRelation = toJoin2;
            innerRelation = toJoin1;
        }


        JoinNode bnlJoinNode = new JoinNode(outerRelation, innerRelation, conditionList, junctionType);
        try {
            bnlJoinNode.annotateSizeNoRecursion();
            bnlJoinNode.normalizeSizeBlocksNoRecursion(cat);
        } catch (SyntaxErrorException ex) { //does not occur at this point.
            ex.printStackTrace();
            System.exit(1);
        }
        JoinNode mJoinNode = new JoinNode(outerRelation, innerRelation, conditionList, junctionType);
        try {
            mJoinNode.annotateSizeNoRecursion();
            mJoinNode.normalizeSizeBlocksNoRecursion(cat);
        } catch (SyntaxErrorException ex) { //does not occur at this point.
            ex.printStackTrace();
            System.exit(1);
        }
        JoinNode hJoinNode = new JoinNode(outerRelation, innerRelation, conditionList, junctionType);
        try {
            hJoinNode.annotateSizeNoRecursion();
            hJoinNode.normalizeSizeBlocksNoRecursion(cat);
        } catch (SyntaxErrorException ex) { //does not occur at this point.
            ex.printStackTrace();
            System.exit(1);
        }
        int BNLJCost = calculateBlockNestedLoopJoinCost(bnlJoinNode);
        int MJCost = calculateMergeJoinCost(mJoinNode);
        int HJCost = calculateHashJoinCost(hJoinNode);
        int minJoinCost = Math.min(BNLJCost, Math.min(MJCost, HJCost));

        JoinTypeEnum joinType;
        if (MJCost == minJoinCost) {
            return mJoinNode;
        } else if (BNLJCost == minJoinCost) {
            return bnlJoinNode;
        } else {
            return hJoinNode;
        }
    }

    /**
     * Calculate Block Nested Loop Join cost
     *
     * @param jnode the JoinNode
     * @return the Block Nested Loop Join cost of the node
     */
    public int calculateBlockNestedLoopJoinCost(JoinNode jnode) {
        Integer br = jnode.getOuterRelation().getSizeBlocks();
        Integer bs = jnode.getInnerRelation().getSizeBlocks();
        Integer availMemoryBuffers = cat.getMemoryAmountOfBuffers() - 2;
        Attribute innerRelSort = jnode.getInnerRelation().getResultSortedOn();
        Attribute outerRelSort = jnode.getOuterRelation().getResultSortedOn();

        //reset inner/outer relations
        jnode.getOuterRelation().setExternalMergeSortResults(false);
        jnode.getInnerRelation().setExternalMergeSortResults(false);
        //
        jnode.setJoinType(JoinTypeEnum.BLOCK_NESTED_LOOP_JOIN);
        jnode.setResultSortedOn(outerRelSort);
//        System.out.println(jnode.getConditionList().get(0).getFirstAttribute());
        if (outerRelSort != null && outerRelSort.equals(jnode.getRequestsSortOn())) {
            jnode.setPipelineable(true); //no extra read cost.
            jnode.setCostRead(0);
        }
        else {
            jnode.setCostRead(br * cat.getDiskTransferTimeMillisecs()
                    + 1 * cat.getDiskAccessLatencyMillisecs());
        }
        if (((bs * cat.getMemoryFrameSizeBytes())
                / ((cat.getMemoryAmountOfBuffers() - 2) * cat.getDiskPageSizeBytes())) == 0) {
            jnode.setFitInnerMemory(true);
            if (innerRelSort != null && innerRelSort.equals(jnode.getRequestsSortOnRight())) {
            }
            else {
                jnode.setCostRead(jnode.getCostRead() + bs * cat.getDiskTransferTimeMillisecs()
                        + 1 * cat.getDiskAccessLatencyMillisecs());
            }
        }
        else {
            Integer blockingUnit = br / availMemoryBuffers + ((br % availMemoryBuffers == 0) ? 0 : 1);
            jnode.setCostRead(jnode.getCostRead() + blockingUnit * bs * cat.getDiskTransferTimeMillisecs()
                    + 2 * cat.getDiskAccessLatencyMillisecs());
        }
        jnode.setCostWriteFinal(jnode.getSizeBlocks() * cat.getDiskTransferTimeMillisecs() + cat.getDiskAccessLatencyMillisecs());
        return jnode.getTotalCost();
    }

    /**
     * Calculate Merge Join cost
     *
     * @param jnode the JoinNode
     * @return the Merge Join cost of the node
     */
    public int calculateMergeJoinCost(JoinNode jnode) {
        TreeNode r = jnode.getOuterRelation();
        TreeNode s = jnode.getInnerRelation();
        int br = r.getSizeBlocks();
        int bs = s.getSizeBlocks();
        int m = cat.getMemoryAmountOfBuffers();
        int costRead = 0;
        //reset inner/outer relations
        r.setExternalMergeSortResults(false);
        s.setExternalMergeSortResults(false);
        //
        jnode.setJoinType(JoinTypeEnum.MERGE_JOIN);
        jnode.setCostWriteFinal(jnode.getSizeBlocks() * cat.getDiskTransferTimeMillisecs() + cat.getDiskAccessLatencyMillisecs());
        if (jnode.getConditionList().size() == 1) {
            Attribute sortAttr = jnode.getRequestsSortOn();
            if (!sortAttr.equals(r.getResultSortedOn())) {
                int nextIntlog = nextIntegerlog(m - 1, br / m);
//                if (((bs * cat.getMemoryFrameSizeBytes())
//                     / ((m - 2) * cat.getDiskPageSizeBytes())) == 0) {
//                   jnode.setOuterFitsMemory(true);
//                }
//                else {
                    r.setExternalMergeSortResults(true);
                    costRead += br * (2 * nextIntlog + 1) * cat.getDiskTransferTimeMillisecs()
                            + (2 * (br / m + (br % m == 0 ? 0 : 1))
                            + (2 * nextIntlog - 1)) * cat.getDiskAccessLatencyMillisecs();
//                }
                costRead += bs * cat.getDiskTransferTimeMillisecs() + cat.getDiskAccessLatencyMillisecs();
            }
            else
                jnode.setPipelineable(true);
            if (!sortAttr.equals(s.getResultSortedOn())) {
                s.setExternalMergeSortResults(true);
                int nextIntlog = nextIntegerlog(m - 1, bs / m);
                if (((br * cat.getMemoryFrameSizeBytes())
                     / ((m - 2) * cat.getDiskPageSizeBytes())) == 0) {
                   jnode.setFitInnerMemory(true);
                }
                else {
                    costRead += bs * (2 * nextIntlog + 1) * cat.getDiskTransferTimeMillisecs()
                            + (2 * (bs / m + (bs % m == 0 ? 0 : 1))
                            + (2 * nextIntlog - 1)) * cat.getDiskAccessLatencyMillisecs();
                }
                costRead += br * cat.getDiskTransferTimeMillisecs() + cat.getDiskAccessLatencyMillisecs();
            }
            else
                jnode.setPipelineableInner(true);
            jnode.setCostRead(costRead);
            return jnode.getTotalCost();
        } else
            return Integer.MAX_VALUE;
    }

    /**
     * Calculate Hash Join cost
     *
     * @param jnode the JoinNode
     * @return the Hash Join cost of the node
     */
    public int calculateHashJoinCost(JoinNode jnode) {
        final double fudgeFactor = 1.2;
        Integer bs = jnode.getInnerRelation().getSizeBlocks();
        Integer br = jnode.getOuterRelation().getSizeBlocks();
        int m = cat.getMemoryAmountOfBuffers() * cat.getMemoryFrameSizeBytes() / cat.getDiskPageSizeBytes();

        //reset inner/outer relations
        jnode.getOuterRelation().setExternalMergeSortResults(false);
        jnode.getInnerRelation().setExternalMergeSortResults(false);
        //
        jnode.setCostWriteFinal(jnode.getSizeBlocks() * cat.getDiskTransferTimeMillisecs() + cat.getDiskAccessLatencyMillisecs());
        jnode.setJoinType(JoinTypeEnum.HASH_JOIN);
        if (m > bs + br) { //entire build input can be kept in main memory
            jnode.setCostRead((br + bs) * cat.getDiskTransferTimeMillisecs() + 2 * cat.getDiskAccessLatencyMillisecs());
            return jnode.getTotalCost();
        }
        int n = (int) ((((double) bs / m) + ((bs % m) == 0 ? 0.0D : 1.0D)) * fudgeFactor);
        if (n < m) { //no recursive partitioning needed
            jnode.setCostRead((br + bs) * cat.getDiskTransferTimeMillisecs() + 2 * cat.getDiskAccessLatencyMillisecs());
            jnode.setCostIntermediate(2 * (br + bs) * cat.getDiskTransferTimeMillisecs()
                    + 2 * ((br / n + (br % n == 0 ? 0 : 1)) + (br / n + (br % n == 0 ? 0 : 1)))
                    * cat.getDiskAccessLatencyMillisecs());
            return jnode.getTotalCost();
        } else { //recursive partitioning needed
            int numPasses = nextIntegerlog(m - 1, bs) - 1;
            jnode.setCostRead((br + bs) * cat.getDiskTransferTimeMillisecs() + 2 * cat.getDiskAccessLatencyMillisecs());
            jnode.setCostIntermediate((2 * (br + bs) * numPasses + br + bs) * cat.getDiskTransferTimeMillisecs()
                    + 2 * ((br / n + (br % n == 0 ? 0 : 1)) + (br / n + (br % n == 0 ? 0 : 1)))
                    * numPasses * cat.getDiskAccessLatencyMillisecs());
            return jnode.getTotalCost();
        }
    }

    public List<Set<TreeNode>> subsets(Set<TreeNode> S) {
        if (S == null) {
            return null;
        }

        TreeNode[] arrayRepr = new TreeNode[S.size()];

        S.toArray(arrayRepr);

        List<Set<TreeNode>> result = new ArrayList<>();

        for (int i = 0; i < S.size(); i++) {
            List<Set<TreeNode>> temp = new ArrayList<>();

            // get sets that are already in result
            for (Set<TreeNode> a : result) {
                temp.add(new LinkedHashSet<>(a));
            }

            // add S[i] to existing sets
            for (Set<TreeNode> a : temp) {
                a.add(arrayRepr[i]);
            }

            // add S[i] only as a set
            Set<TreeNode> single = new LinkedHashSet<>();

            single.add(arrayRepr[i]);
            temp.add(single);
            result.addAll(temp);
        }

        // add empty set
        // result.add(new ArrayList<>());
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).size() == S.size()) {
                result.remove(i);
            }
        }

        return result;
    }
}