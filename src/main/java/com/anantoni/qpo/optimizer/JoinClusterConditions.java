package com.anantoni.qpo.optimizer;

import com.anantoni.qpo.query_tree.Condition;
import com.anantoni.qpo.query_tree.JoinTypeEnum;
import com.anantoni.qpo.query_tree.JunctionEnum;

import java.util.List;

/**
 * Created by anantoni on 18/3/2015.
 */
public class JoinClusterConditions {
    private final List<Condition> conditionList;
    private final JunctionEnum junctionType;

    public JoinClusterConditions(List<Condition> conditionList, JunctionEnum junctionType) {
        this.conditionList = conditionList;
        this.junctionType = junctionType;
    }

    public List<Condition> getConditionList() {
        return conditionList;
    }

    public JunctionEnum getJoinType() {
        return junctionType;
    }

    public void print() {
        switch (junctionType) {
            case AND:
                System.out.println("AND");
                break;
            case OR:
                System.out.println("OR");
                break;
            case NONE:
                System.out.println("NONE");
                break;
        }
    }

}
