package com.anantoni.qpo.optimizer;


//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.exception.UnsanitizableException;
import com.anantoni.qpo.parser.ParseException;
import com.anantoni.qpo.parser.RelAlgParser;
import com.anantoni.qpo.query_tree.RootNode;
import com.anantoni.qpo.query_tree.TreeNode;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;
import com.anantoni.qpo.visitor.QueryTreeGenerator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import syntaxtree.Query;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class Driver {
    public static void main(String[] args) {

        TreePrinter treePrinter = new TreePrinter();
        BufferedReader br = null;

        /**
         * Execute Relation Algebra parser to generate AST
         * Traverse the generated AST with the QueryTreeGenerator visitor
         * to generate the unoptimized query tree.
         */
        try {
            if (args.length != 1) {
                System.err.println("Please give input file");
                System.exit(-1);
            }

            RelAlgParser parser = new RelAlgParser(new FileReader(args[0]));
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            br = new BufferedReader(new FileReader("catalog-dir/freshdirect.json"));

            Query query = parser.Query();
            TreeNode queryTreeRoot = new RootNode(query.accept(new QueryTreeGenerator(), null));
            Catalog cat = gson.fromJson(br, Catalog.class);
            Optimizer opt = new Optimizer(queryTreeRoot, cat);

            /**
             * Initial plan
             */
            opt.init();
            treePrinter.printTree(queryTreeRoot, "Initial Plan");

            /**
             * Push selections
             */
            opt.pushSelections();

            /**
             * Push projections
             */

            /**
             * Annotate and normalize sizes of TreeNodes
             */
            queryTreeRoot.annotateSize();
            queryTreeRoot.normalizeSizeBlocks(cat);
            treePrinter.printTree(queryTreeRoot, "Pre-join opt Plan");
            opt.costEstimation();
            /**
             * Optimize joins
             */
            //opt.optimizeJoins();
            opt.optimizeMultiClusterJoins();

            // queryTreeRoot.commandLinePrint();
            // System.out.println(gson.toJson(cat, Catalog.class));
            treePrinter.printTree(queryTreeRoot, "Final Plan");
            treePrinter.presentGraph();
        } catch (FileNotFoundException | ParseException ex) {
            Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsanitizableException ex) {
            System.err.println("Unsanitizable Exception: " + ex.getMessage());
            ex.printStackTrace();
        } catch (UnknownRelationException ex) {
            System.err.println(ex.getMessage());
        } catch (SyntaxErrorException ex) {
            System.err.println("Syntax Error: " + ex.getMessage());
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}