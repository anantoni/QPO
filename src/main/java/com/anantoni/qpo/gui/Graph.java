package com.anantoni.qpo.gui;

//~--- JDK imports ------------------------------------------------------------

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Line2D;

import javax.swing.JApplet;
import javax.swing.JFrame;

public class Graph extends JApplet {
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(Color.gray);

        int x = 5;
        int y = 7;

        g2.draw(new Line2D.Double(x, y, 200, 200));
        g2.drawString("Line", x, 250);
    }

    @Override
    public void init() {
        JFrame f = new JFrame("Query Graph");

        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        JApplet applet = new Graph();

        f.getContentPane().add("Center", applet);
        setBackground(Color.white);
        setForeground(Color.white);
        f.pack();
        f.setSize(new Dimension(1024, 768));
        f.setVisible(true);
    }
}
