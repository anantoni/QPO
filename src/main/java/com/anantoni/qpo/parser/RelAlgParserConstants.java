/* Generated By:JavaCC: Do not edit this line. RelAlgParserConstants.java */
package com.anantoni.qpo.parser;

/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface RelAlgParserConstants {

    /**
     * End of File.
     */
    int EOF = 0;

    /**
     * RegularExpression Id.
     */
    int LPAREN = 6;

    /**
     * RegularExpression Id.
     */
    int RPAREN = 7;

    /**
     * RegularExpression Id.
     */
    int LSQBR = 8;

    /**
     * RegularExpression Id.
     */
    int RSQBR = 9;

    /**
     * RegularExpression Id.
     */
    int SEL = 10;

    /**
     * RegularExpression Id.
     */
    int PROJ = 11;

    /**
     * RegularExpression Id.
     */
    int JOIN = 12;

    /**
     * RegularExpression Id.
     */
    int PLUS = 13;

    /**
     * RegularExpression Id.
     */
    int MINUS = 14;

    /**
     * RegularExpression Id.
     */
    int TIMES = 15;

    /**
     * RegularExpression Id.
     */
    int DIVIDE = 16;

    /**
     * RegularExpression Id.
     */
    int MOD = 17;

    /**
     * RegularExpression Id.
     */
    int EQ = 18;

    /**
     * RegularExpression Id.
     */
    int NE = 19;

    /**
     * RegularExpression Id.
     */
    int GE = 20;

    /**
     * RegularExpression Id.
     */
    int LE = 21;

    /**
     * RegularExpression Id.
     */
    int GT = 22;

    /**
     * RegularExpression Id.
     */
    int LT = 23;

    /**
     * RegularExpression Id.
     */
    int UNION = 24;

    /**
     * RegularExpression Id.
     */
    int INTER = 25;

    /**
     * RegularExpression Id.
     */
    int DIFF = 26;

    /**
     * RegularExpression Id.
     */
    int DOT = 27;

    /**
     * RegularExpression Id.
     */
    int OR = 28;

    /**
     * RegularExpression Id.
     */
    int AND = 29;

    /**
     * RegularExpression Id.
     */
    int NOT = 30;

    /**
     * RegularExpression Id.
     */
    int SEMI = 31;

    /**
     * RegularExpression Id.
     */
    int DQ = 32;

    /**
     * RegularExpression Id.
     */
    int COMMA = 33;

    /**
     * RegularExpression Id.
     */
    int INTEGER_LITERAL = 34;

    /**
     * RegularExpression Id.
     */
    int IDENTIFIER = 35;

    /**
     * RegularExpression Id.
     */
    int STRING_LITERAL = 36;

    /**
     * RegularExpression Id.
     */
    int LETTER = 37;

    /**
     * RegularExpression Id.
     */
    int DIGIT = 38;

    /**
     * Lexical state.
     */
    int DEFAULT = 0;

    /**
     * Literal token values.
     */
    String[] tokenImage = {
            "<EOF>", "\" \"", "\"\\t\"", "\"\\n\"", "\"\\r\"", "\"\\f\"", "\"(\"", "\")\"", "\"[\"", "\"]\"", "\"sel\"",
            "\"proj\"", "\"join\"", "\"+\"", "\"-\"", "\"*\"", "\"/\"", "\"%\"", "\"=\"", "\"!=\"", "\">=\"", "\"<=\"",
            "\">\"", "\"<\"", "\"union\"", "\"inter\"", "\"diff\"", "\".\"", "\"OR\"", "\"AND\"", "\"NOT\"", "\";\"",
            "\"\\\"\"", "\",\"", "<INTEGER_LITERAL>", "<IDENTIFIER>", "<STRING_LITERAL>", "<LETTER>", "<DIGIT>",
    };
}


//~ Formatted by Jindent --- http://www.jindent.com
