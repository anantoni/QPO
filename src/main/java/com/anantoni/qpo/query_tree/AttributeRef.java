/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

/**
 * @author anantoni
 */
public class AttributeRef implements Operand {
    private final String relation;
    private final String attrName;

    public AttributeRef(String textRepr) {

        if (textRepr.contains(".")) {
            String[] pieces = textRepr.split("\\.");

            relation = pieces[0];
            attrName = pieces[1];
        } else {
            relation = null;
            attrName = textRepr;
        }
    }

    @Override
    public String toString() {
        if (relation != null) {
            return getAttrName();
        } else {
            return getAttrName();
        }
    }

    /**
     * @return the relation
     */
    public String getRelation() {
        return relation;
    }

    /**
     * @return the attrName
     */
    public String getAttrName() {
        return attrName;
    }

    @Override
    public Integer getAsNumericalValue() {
        return null;
    }

    @Override
    public String getAttributeName() {
        return attrName;
    }

    @Override
    public String getRelationName() {
        return relation;
    }
}
