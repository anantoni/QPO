package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;

import java.util.Set;

//~--- JDK imports ------------------------------------------------------------

public class Condition {
    private Operand fstOperand;
    private CompEnum comparator;
    private Operand sndOperand;
    private Attribute firstAttribute;
    private Attribute secondAttribute;
    private String comp;

    public Condition(Operand fstOperand, String comparator, Operand sndOperand) {
        this.fstOperand = fstOperand;
        this.comp = comparator;

        switch (comparator) {
            case "=":
                this.comparator = CompEnum.EQUALS;
                break;

            case ">=":
                this.comparator = CompEnum.GREATER_OR_EQUAL;
                break;

            case "<=":
                this.comparator = CompEnum.LESS_OR_EQUAL;
                break;

            case ">":
                this.comparator = CompEnum.GREATER_THAN;
                break;

            case "<":
                this.comparator = CompEnum.LESS_THAN;
                break;

            case "!=":
                this.comparator = CompEnum.NOT_EQUAL_TO;
                break;
        }
        this.sndOperand = sndOperand;
    }

    @Override
    public String toString() {
        return this.fstOperand + this.comp + this.sndOperand;
    }

    /**
     * @return the fstOperand
     */
    public Operand getFstOperand() {
        return fstOperand;
    }

    /**
     * @param fstOperand the fstOperand to set
     */
    public void setFstOperand(Operand fstOperand) {
        this.fstOperand = fstOperand;
    }

    /**
     * @return the comparator
     */
    public CompEnum getComparator() {
        return comparator;
    }

    /**
     * @param comparator the comparator to set
     */
    public void setComparator(CompEnum comparator) {
        this.comparator = comparator;
    }

    /**
     * @return the sndOperand
     */
    public Operand getSndOperand() {
        return sndOperand;
    }

    /**
     * @param sndOperand the sndOperand to set
     */
    public void setSndOperand(Operand sndOperand) {
        this.sndOperand = sndOperand;
    }

    /**
     * @return the comp
     */
    public String getComp() {
        return comp;
    }

    /**
     * @param comp the comp to set
     */
    public void setComp(String comp) {
        this.comp = comp;
    }

    public void attributeBuildPass(Set<Attribute> attributes) throws SyntaxErrorException {
        if (sndOperand.getAttributeName() != null) {
            for (Attribute attr : attributes) {
                if (attr.getName().equals(fstOperand.getAttributeName())) {
                    firstAttribute = attr;
                }

                if (attr.getName().equals(sndOperand.getAttributeName())) {
                    secondAttribute = attr;
                }
            }
            if (firstAttribute == null || secondAttribute == null)
                throw new SyntaxErrorException("Invalid operands in condition: " + toString());
        }
        else {
            for (Attribute attr : attributes) {
                if (attr.getName().equals(fstOperand.getAttributeName())) {
                    firstAttribute = attr;
                }
            }
            if (firstAttribute == null)
                throw new SyntaxErrorException("Invalid operand in condition: " + toString());
        }
    }

    void attributeBuildPass(Set<Attribute> attributesL, Set<Attribute> attributesR) throws SyntaxErrorException  {
        for (Attribute attr : attributesL) {
            if (attr.getName().equals(fstOperand.getAttributeName())) {
                firstAttribute = attr;
            }
        }
        for (Attribute attr : attributesR) {
            if (attr.getName().equals(sndOperand.getAttributeName())) {
                secondAttribute = attr;
            }
        }
        if (firstAttribute == null || secondAttribute == null)
            throw new SyntaxErrorException("Invalid operands in condition: " + toString());
    }

    public Attribute getFirstAttribute() {
        return firstAttribute;
    }

    public Attribute getSecondAttribute() {
        return secondAttribute;
    }

    public boolean equals(Condition other) {
        if (this.firstAttribute == null)
            System.out.println("First attribute null");
        if (this.secondAttribute == null)
            System.out.println("Second attribute null");
        return this.firstAttribute.equals(other.getFirstAttribute())
                && this.secondAttribute.equals(other.getSecondAttribute())
                && this.comp.equals(other.getComp());
    }

    public void removeRelationPrefix() {
        this.firstAttribute.removeRelationPrefix();
        this.secondAttribute.removeRelationPrefix();
    }
}
