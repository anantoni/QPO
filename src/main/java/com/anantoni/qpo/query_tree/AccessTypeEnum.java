package com.anantoni.qpo.query_tree;


public enum AccessTypeEnum {
    LINEAR_SCAN,
    PRIMARY_INDEX_KNIFE,
    PRIMARY_INDEX_SCAN,
    DESCEND_INDEX_SCAN
}
