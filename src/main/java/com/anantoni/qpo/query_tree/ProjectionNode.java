/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.optimizer.Optimizer;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.*;

import static com.anantoni.qpo.query_tree.AccessTypeEnum.LINEAR_SCAN;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class ProjectionNode extends TreeNode {
    private final List<String> attributeList;
    private TreeNode expr;

    @SuppressWarnings("empty-statement")
    public ProjectionNode(List<String> attributeList, TreeNode expr) {
        this.attributeList = attributeList;

        while (attributeList.remove(",")) ;
        this.expr = expr;
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        if (father != null) {
            this.getParents().add(father);
        }

        return getExpr().referenceBuildPass(catalog, this);
    }

    @Override
    public void commandLinePrint() {
        printNodeInfo();
        getExpr().commandLinePrint();
    }

    @Override
    public void printNodeInfo() {
        for (String attribute : getAttributeList()) {
            System.out.print(" " + attribute + " ");
        }

        System.out.println("");
    }

    @Override
    public void printDotNotation(PrintWriter pw) {
        super.printDotNotation(pw);
        pw.print("[label=\"Π(");

        ListIterator<String> li = getAttributeList().listIterator();

        while (li.hasNext()) {
            if (li.nextIndex() != (getAttributeList().size() - 1)) {
                String s = li.next();

                pw.print(s + ",");
            } else {
                String s = li.next();

                pw.print(s);
            }
        }
        pw.print(")");
        if (resultSortedOn != null) {
            pw.print("\\nSorted On: " + resultSortedOn.getName());
        }
        if (requestsSortOn != null) {
            pw.print("\\nReqSort: " + requestsSortOn.getName());
        }
        if (requestsSortOnRight != null) {
            pw.print("\\nReqSortR: " + requestsSortOnRight.getName());
        }
        if (costRead != null)
            pw.print("\\nCost: " + (pipelineable ? 0 : costRead) + "ms");
        pw.println("\" shape=box]");
        printDotNotationEdge(getExpr(), pw);
        getExpr().printDotNotation(pw);
    }

        @Override
    protected void printDotNotationEdge(TreeNode other, PrintWriter pw) {
        pw.print("a" + getGraphID() + getDotNotationId() + "->a"
                 + getGraphID() + other.getDotNotationId()
                 + " [color = \"brown\" dir=\"none\" label=\"");
        if (other.sizeBlocks != null) {
            pw.print("Size(blocks): " + other.sizeBlocks);
        }
        else if (other.numRecords != null) {
            pw.print("Size(recs): " + other.numRecords);
        }
        if (other.externalMergeSortResults) {
            pw.print("\\n--External Merge-Sort--");
        }
        else if (pipelineable)
            pw.print("\\n--Pipelined--");
        if (!pipelineable && other.costWriteFinal != null)
            pw.print("\\Write Cost: " + other.costWriteFinal + "ms");
        pw.println("\"]");
    }
    
    @Override
    public void selectionSplit() {
        this.getExpr().selectionSplit();
    }

    /**
     * @return
     */
    @Override
    public Set<Relation> getRelations() {
        return getExpr().getRelations();
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
        if (this.getExpr().getClass().equals(childType)) {
            parentList.add(this);
        }

        this.getExpr().addListNodeParentsDFS(childType, parentList);
    }

    public void selectionPushdown() {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectionPushdownInit() {
        if (this.expr instanceof SelectionNode
                && !(this.expr.getExpr() instanceof SelectionNode
                || this.expr.getExpr() instanceof RelationNode))
        {
            TreeNode selectionNode = this.expr;
            TreeNode childNode = this.expr.getExpr();

            this.setExpr(childNode);
            this.expr.selectionPushdown(selectionNode);
        }
    }

    @Override
    public void selectionPushdown(TreeNode selectionNode) {
        Set<Relation> exprSet = this.expr.getRelations();

        for (Relation rel : exprSet) {
            if ((this.expr instanceof RelationNode) || (this.expr instanceof SelectionNode)) {
                TreeNode temp = this.expr;

                this.expr = selectionNode;
                selectionNode.setExpr(temp);
            } else {
                this.expr.selectionPushdown(selectionNode);
            }

            break;
        }
    }

    /**
     * @return the expr
     */
    public TreeNode getExpr() {
        return expr;
    }

    /**
     * @param expr the expr to set
     */
    @Override
    public void setExpr(TreeNode expr) {
        this.expr = expr;
    }

    /**
     * @return the attributeList
     */
    public List<String> getAttributeList() {
        return attributeList;
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        Set<TreeNode> retVal = new HashSet<>();

        retVal.add(expr);

        return retVal;
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {
        if (expr == oldChild) {
            expr = newChild;
        } else {
            System.err.println("Unknown child");
        }
    }

    @Override
    public int annotateSize() throws SyntaxErrorException {
        expr.annotateSize();
        setNumRecords(0);

        setAttributes(new HashSet<>());

        for (String attrName : attributeList) {
            boolean bool = false;

            for (Attribute attr : expr.getAttributes()) {
                if (bool = attrName.substring(attrName.indexOf('.') + 1).equals(attr.getName())) {
                    getAttributes().add(new Attribute(attr));

                    break;
                }
            }

            if (!bool)
                throw new SyntaxErrorException("Attribute \"" + attrName + "\" not found in Projection");
        }
        setNumRecords(1);
        for (Attribute attr : getAttributes()) {
            Integer uniqueVals = attr.getUniqueValues();

            if (uniqueVals == null) {
                setNumRecords(expr.getSize());    // if unavailable, estimate is the same as the size of expression
                return getNumRecords();
            }
            setNumRecords(getNumRecords() * uniqueVals);
            if (getNumRecords() > expr.getSize()) {
                setNumRecords(expr.getSize());
                return getNumRecords();
            }

        }
        return getNumRecords();
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        expr.annotateCost(catalog);
        setReadAlgorithm(LINEAR_SCAN);
        setCostRead(expr.getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
        setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() * 2 + catalog.getDiskAccessLatencyMillisecs());
        if (!expr.externalMergeSortResults && !(expr instanceof RelationNode)) {
            pipelineable = true;
            externalMergeSortResults = true;
        }
        else {
            if (expr instanceof RelationNode)
                externalMergeSortResults = true;
            int m = catalog.getMemoryAmountOfBuffers();
            int br = sizeBlocks;
            int nextIntloR = Optimizer.nextIntegerlog(m - 1, br / m);
            costRead += br * (2 * nextIntloR + 1) * catalog.getDiskTransferTimeMillisecs()
                    + (2 * (br / m + (br % m == 0 ? 0 : 1))
                    + (2 * nextIntloR - 1)) * catalog.getDiskAccessLatencyMillisecs();
        }
        catalog.getDiskAccessLatencyMillisecs();
        return costWriteFinal;
    }

    public void addJoinNodeParentsDFS(Map<TreeNode, List<Condition>> parentList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        this.expr.getJoinNodeCluster(commonConditions, joinNodeCluster);
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {
        this.expr.fillMultiClusterMap(multiClusterMap);
    }
}
