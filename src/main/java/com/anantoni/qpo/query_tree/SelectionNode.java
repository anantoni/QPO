/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Key;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.*;

import static com.anantoni.qpo.query_tree.AccessTypeEnum.LINEAR_SCAN;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class SelectionNode extends TreeNode {
    private final List<Condition> conditionList;
    private final JunctionEnum junctionType;
    private TreeNode expr;
    private Set<Relation> relations;

    public SelectionNode(Condition condition) {
        this.conditionList = new ArrayList<>();
        this.conditionList.add(condition);
        this.junctionType = JunctionEnum.AND;
    }

    @SuppressWarnings("empty-statement")
    public SelectionNode(List<String> conditionList) {
        this.conditionList = new ArrayList<>();

        String junction = "";

        /**
         * Input condition list format is op11, comp, op12 (, {AND|OR}, op21, comp, op22)
         * We need to identify junction type
         */
        if (conditionList.size() > 3) {
            junction = conditionList.get(3);
        }

        if (junction.equals("AND")) {
            this.junctionType = JunctionEnum.AND;
        } else {
            this.junctionType = JunctionEnum.OR;
        }

        // Remove junction strings from list
        while (conditionList.remove(junction)) ;

        ListIterator<String> li = conditionList.listIterator();

        while (li.hasNext()) {
            String firstOperand = li.next();
            String comp = li.next();
            String secondOperand = li.next();
            Operand sndOperand;

            if (secondOperand.contains("\"")) {
                sndOperand = new StringLiteral(secondOperand);
            } else if (IntegerLiteral.isInteger(secondOperand)) {
                sndOperand = new IntegerLiteral(secondOperand);
            } else {
                sndOperand = new AttributeRef(secondOperand);
            }

            this.conditionList.add(new Condition(new AttributeRef(firstOperand), comp, sndOperand));
        }
    }

    public SelectionNode(SelectionNode another) {
        this.expr = another.expr;
        this.relations = another.relations;
        this.conditionList = another.conditionList;
        this.junctionType = another.junctionType;
    }

    public SelectionNode(List<String> conditionList, TreeNode expr) {
        this(conditionList);
        this.expr = expr;

//      System.out.println("Number of conditions: " + conditionList.size()/3);
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        if (father != null) {
           this.parents.add(father);
        }
        if (this.expr == null)
            System.err.println("IS NULL");
        return this.expr.referenceBuildPass(catalog, this);
    }

    @Override
    public void commandLinePrint() {
        printNodeInfo();
        getExpr().commandLinePrint();
    }

    @Override
    public void printNodeInfo() {
        System.out.print("SelectionNode");

        for (Condition condition : getConditionList()) {
            System.out.println(condition.toString());
        }

        System.out.print(" ");

        if (getJunctionType() == JunctionEnum.AND) {
            System.out.println("Conjunction");
        } else {
            System.out.println("Disjunction");
        }
    }

    @Override
    public void printDotNotation(PrintWriter pw) {
        char junction;

        super.printDotNotation(pw);
        pw.print("[label=\"σ(");

        if (getJunctionType() == JunctionEnum.AND) {
            junction = '∧';
        } else if (getJunctionType() == JunctionEnum.OR) {
            junction = '∨';
        } else {
            junction = ' ';
        }

        ListIterator<Condition> li = getConditionList().listIterator();

        while (li.hasNext()) {
            if (li.nextIndex() != (getConditionList().size() - 1)) {
                pw.print(li.next().toString() + junction);
            } else {
                pw.print(li.next().toString());
            }
        }
        pw.print(")");
        if (resultSortedOn != null) {
            pw.print("\\nSorted On: " + resultSortedOn.getName());
        }
        if (costRead != null)
            pw.print("\\nCost: " + (pipelineable ? 0 : costRead) + "ms");
        pw.println("\" shape=box]");
        printDotNotationEdge(expr, pw);
        expr.printDotNotation(pw);
    }
    
    @Override
    protected void printDotNotationEdge(TreeNode other, PrintWriter pw) {
        pw.print("a" + getGraphID() + getDotNotationId() + "->a"
                 + getGraphID() + other.getDotNotationId()
                 + " [color = \"brown\" dir=\"none\" label=\"");
        if (other.sizeBlocks != null) {
            pw.print("Size(blocks): " + other.sizeBlocks);
        }
        else if (other.numRecords != null) {
            pw.print("Size(recs): " + other.numRecords);
        }
        if (other.externalMergeSortResults) {
            pw.print("\\n--External Merge-Sort--");
        }
        else if (pipelineable) {
                pw.print("\\n--Pipelined--");
        }
        if (!pipelineable && other.costWriteFinal != null)
            pw.print("\\Write Cost: " + other.costWriteFinal + "ms");
        if (other instanceof RelationNode && readAlgorithm != null) {
            //assumes primary key, TODO: when extended, verify by if (other.resultsSortedOn.equals(requestsSortOn)) ...
            switch (readAlgorithm) {
                case LINEAR_SCAN: pw.print("\\n--Scan Linearly--"); break;
                case DESCEND_INDEX_SCAN: pw.print("\\n--Descend index to leftmost and Scan--"); break;
                case PRIMARY_INDEX_KNIFE: pw.print("\\n--Use Index to retrieve--"); break;
                case PRIMARY_INDEX_SCAN: pw.print("\\n--Use Index and Scan--"); break;
            }
        }
        pw.println("\"]");
    }

    @Override
    public void selectionSplit() {
        if (this.getJunctionType() == JunctionEnum.AND) {
            if (this.getConditionList().size() > 1) {
                TreeNode childNode = this.expr;
                TreeNode currentNode = this;
                ListIterator<Condition> li = this.getConditionList().listIterator(1);

                while (li.hasNext()) {
                    currentNode.setExpr(new SelectionNode(li.next()));
                    currentNode = currentNode.getExpr();
                    li.remove();
                }

                currentNode.setExpr(childNode);
                currentNode.getExpr().selectionSplit();
            }
        } else {
            this.expr.selectionSplit();
        }
    }

    @Override
    public Set<Relation> getRelations() {
        return getExpr().getRelations();
    }

    /**
     * @param relations the relations to set
     */
    public void setRelations(Set<Relation> relations) {
        this.relations = relations;
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
        if (this.getExpr().getClass().equals(childType)) {
            parentList.add(this);
        }

        this.getExpr().addListNodeParentsDFS(childType, parentList);
    }

    /**
     * @param selectionNode
     */
    @Override
    public void selectionPushdown(TreeNode selectionNode) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectionPushdownInit() {
        if (this.expr instanceof SelectionNode
                && !(this.expr.getExpr() instanceof SelectionNode
                || this.expr.getExpr() instanceof RelationNode))
        {
            TreeNode selectionNode = this.expr;
            TreeNode childNode = this.expr.getExpr();

            this.setExpr(childNode);
            this.expr.selectionPushdown(selectionNode);
        }
    }

    /**
     * @return the expr
     */
    public TreeNode getExpr() {
        return expr;
    }

    @Override
    public void setExpr(TreeNode expr) {
        this.expr = expr;
    }

    /**
     * @return the conditionList
     */
    public List<Condition> getConditionList() {
        return conditionList;
    }

    /**
     * @return the junctionType
     */
    public JunctionEnum getJunctionType() {
        return junctionType;
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        Set<TreeNode> retVal = new HashSet<>();

        retVal.add(expr);

        return retVal;
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {
        if (expr == oldChild) {
            expr = newChild;
        } else {
            System.err.println("Unrelated child");
        }
    }

    @Override
    public int annotateSize() throws SyntaxErrorException {
        int numRecs = expr.annotateSize();
        Integer value, uniqueVals;
        double condSelectivity, sized, condSelectivitySnd;

        sized = numRecs;

        setAttributes(new HashSet<>());
        for (Attribute attr : expr.getAttributes()) {
            getAttributes().add(new Attribute(attr));
        }

        for (Condition cond : conditionList) {
            cond.attributeBuildPass(expr.getAttributes());
        }
        switch (junctionType) {
            // NONE's position (w/ AND vs w/ OR) is irrelevant, since
            //it's estimated correctly for both other cases. (only one condition)
            case NONE: 
            case AND:
                for (Condition cond : conditionList) {
                    switch (cond.getComparator()) {
                        case EQUALS:
                            if (expr instanceof RelationNode) {
                                Attribute attr = ((RelationNode)expr).getRelation().getPrimaryKey().getDomains().get(0);
                                if (cond.getFirstAttribute().equals(attr)
                                    || (cond.getSecondAttribute() != null
                                        && cond.getSecondAttribute().equals(attr))) {
                                    numRecords = 1;
                                    return 1; //conjunction, equality on primary key, all done here.
                                }
                            }
                            uniqueVals = cond.getFirstAttribute().getUniqueValues();

                            if (uniqueVals != null) {
                                condSelectivity = 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs / uniqueVals) / numRecs
                            } else {
                                condSelectivity = 0.5D;    // (numRecs / 2) / numRecs where no stats available
                            }

                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {    // has second attribute, not value:
                                uniqueVals = cond.getSecondAttribute().getUniqueValues();

                                if (uniqueVals != null) {
                                    condSelectivitySnd = 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs / uniqueVals) / numRecs
                                } else {
                                    condSelectivitySnd = 0.5D;    // (numRecs / 2) / numRecs where no stats available
                                }

                                condSelectivity = (condSelectivitySnd > condSelectivity)
                                        ? condSelectivity
                                        : condSelectivitySnd;
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (((minVal != null) && (value < minVal)) || ((maxVal != null) && (value > maxVal))) {
                                    this.setNumRecords(0);

                                    return 0;
                                }
                            }

                            sized *= condSelectivity;

                            break;

                        case GREATER_OR_EQUAL:
                        case GREATER_THAN:
                            condSelectivity = 1.0D;
                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {                  // has second attribute, not value:
                                condSelectivity *= 0.5D;          // no data distribution info
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (maxVal != null) {
                                    if (value > maxVal) {
                                        this.setNumRecords(0);

                                        return 0;
                                    } else if ((minVal != null) && (value > minVal)) {
                                        condSelectivity *= ((maxVal - (double) value) / (maxVal - minVal));
                                    }
                                } else {                          // not enough stats available
                                    condSelectivity *= 0.5D;
                                }
                            }

                            sized *= condSelectivity;

                            break;

                        case LESS_OR_EQUAL:
                        case LESS_THAN:
                            condSelectivity = 1.0D;
                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {                  // has second attribute, not value:
                                condSelectivity *= 0.5D;          // no data distribution info
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (minVal != null) {
                                    if (value < minVal) {
                                        this.setNumRecords(0);

                                        return 0;
                                    } else if (maxVal != null) {
                                        condSelectivity *= (((double) value - minVal) / (maxVal - minVal));
                                    }
                                } else {                          // not enough stats available
                                    condSelectivity *= 0.5D;
                                }
                            }

                            sized *= condSelectivity;

                            break;

                        case NOT_EQUAL_TO:    // size(selection NOT_EQUAL_TO) = numRecs - size(selection EQUAL_TO) for same operands.
                            uniqueVals = cond.getFirstAttribute().getUniqueValues();

                            if (uniqueVals != null) {
                                condSelectivity = 1.0D - 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs - (numRecs / uniqueVals)) / numRecs
                            } else {
                                condSelectivity = 0.5D;    // (numRecs - (numRecs / 2)) / numRecs where no stats available
                            }

                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {    // has second attribute, not value:
                                uniqueVals = cond.getSecondAttribute().getUniqueValues();

                                if (uniqueVals != null) {
                                    condSelectivitySnd = 1.0D - 1.0D / uniqueVals;    // assuming independence: selectivity is: same as above
                                } else {
                                    condSelectivitySnd = 0.5D;    // (numRecs / 2) / numRecs where no stats available
                                }

                                condSelectivity = (condSelectivitySnd > condSelectivity)
                                        ? condSelectivity
                                        : condSelectivitySnd;
                            }

                            sized *= condSelectivity;

                            break;
                    }
                }

                break;

            case OR:
                double sizePart = 1;                          // size: n * ( 1 - sizePart)

                for (Condition cond : conditionList) {
                    switch (cond.getComparator()) {
                        case EQUALS:
                            uniqueVals = cond.getFirstAttribute().getUniqueValues();

                            if (uniqueVals != null) {
                                condSelectivity = 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs / uniqueVals) / numRecs
                            } else {
                                condSelectivity = 0.5D;    // (numRecs / 2) / numRecs where no stats available
                            }

                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {    // has second attribute, not value:
                                uniqueVals = cond.getSecondAttribute().getUniqueValues();

                                if (uniqueVals != null) {
                                    condSelectivitySnd = 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs / uniqueVals) / numRecs
                                } else {
                                    condSelectivitySnd = 0.5D;    // (numRecs / 2) / numRecs where no stats available
                                }

                                condSelectivity = (condSelectivitySnd > condSelectivity)
                                        ? condSelectivity
                                        : condSelectivitySnd;
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (((minVal != null) && (value < minVal)) || ((maxVal != null) && (value > maxVal))) {
                                    this.setNumRecords(0);

                                    return 0;
                                }
                            }

                            sizePart *= (1 - condSelectivity);

                            break;

                        case GREATER_OR_EQUAL:
                        case GREATER_THAN:
                            condSelectivity = 1.0D;
                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {                  // has second attribute, not value:
                                condSelectivity *= 0.5D;          // no data distribution info
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (maxVal != null) {
                                    if (value > maxVal) {
                                        this.setNumRecords(0);

                                        return 0;
                                    } else if (minVal != null) {
                                        condSelectivity *= ((maxVal - (double) value) / (maxVal - minVal));
                                    }
                                } else {                          // not enough stats available
                                    condSelectivity *= 0.5D;
                                }
                            }

                            sizePart *= (1 - condSelectivity);

                            break;

                        case LESS_OR_EQUAL:
                        case LESS_THAN:
                            condSelectivity = 1.0D;
                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {                  // has second attribute, not value:
                                condSelectivity *= 0.5D;          // no data distribution info
                            } else {                              // second operand is a value
                                Integer minVal = cond.getFirstAttribute().getMinValue();
                                Integer maxVal = cond.getFirstAttribute().getMaxValue();

                                if (minVal != null) {
                                    if (value < minVal) {
                                        this.setNumRecords(0);

                                        return 0;
                                    } else if (maxVal != null) {
                                        condSelectivity *= (((double) value - minVal) / (maxVal - minVal));
                                    }
                                } else {                          // not enough stats available
                                    condSelectivity *= 0.5D;
                                }
                            }

                            sizePart *= (1 - condSelectivity);

                            break;

                        case NOT_EQUAL_TO:    // size(selection NOT_EQUAL_TO) = numRecs - size(selection EQUAL_TO) for same operands.
                            uniqueVals = cond.getFirstAttribute().getUniqueValues();

                            if (uniqueVals != null) {
                                condSelectivity = 1.0D - 1.0D / uniqueVals;    // assuming independence: selectivity is: (numRecs - (numRecs / uniqueVals)) / numRecs
                            } else {
                                condSelectivity = 0.5D;                        // (numRecs - (numRecs / 2)) / numRecs where no stats available
                            }

                            value = cond.getSndOperand().getAsNumericalValue();

                            if (value == null) {    // has second attribute, not value:
                                uniqueVals = cond.getSecondAttribute().getUniqueValues();

                                if (uniqueVals != null) {
                                    condSelectivitySnd = 1.0D - 1.0D / uniqueVals;    // assuming independence: selectivity is: same as above
                                } else {
                                    condSelectivitySnd = 0.5D;                        // (numRecs / 2) / numRecs where no stats available
                                }

                                condSelectivity = (condSelectivitySnd > condSelectivity)
                                        ? condSelectivity
                                        : condSelectivitySnd;
                            }

                            sizePart *= (1 - condSelectivity);

                            break;
                    }

                    sized *= (1 - sizePart);
                }
        }

        return this.numRecords = (int) (Math.ceil(sized));
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        expr.annotateCost(catalog);
        CompEnum comp = CompEnum.NOT_EQUAL_TO;
        readAlgorithm = AccessTypeEnum.LINEAR_SCAN;
        if (expr.getResultSortedOn() != null) {
            resultSortedOn = expr.resultSortedOn;
            for (Condition cond : conditionList) {
                if (cond.getComparator() != CompEnum.NOT_EQUAL_TO) {
                    if (expr.getResultSortedOn().equals(cond.getFirstAttribute())) {
                        setRequestsSortOn(expr.getResultSortedOn());
                        comp = cond.getComparator();
                        break;
                    }
                    if (expr.getResultSortedOn().equals(cond.getSecondAttribute())) {
                        setRequestsSortOn(expr.getResultSortedOn());
                        comp = cond.getComparator();
                        if (comp == CompEnum.GREATER_OR_EQUAL)
                            comp = CompEnum.LESS_OR_EQUAL;
                        else if (comp == CompEnum.LESS_OR_EQUAL)
                            comp = CompEnum.GREATER_OR_EQUAL;
                        else if (comp == CompEnum.LESS_THAN)
                            comp = CompEnum.GREATER_THAN;
                        else if (comp == CompEnum.GREATER_THAN)
                            comp = CompEnum.LESS_THAN;
                        break;
                    }
                }
            }
        }
        setCostRead(0);
        Key primKey;
        if (expr instanceof RelationNode) {
            if (getRequestsSortOn() != null) {
                Relation r = ((RelationNode) expr).getRelation();
                primKey = r.getPrimaryKey();
                if (requestsSortOn.equals(primKey.getDomains().get(0))) {
                    switch (comp) {
                        case EQUALS:
                            if (requestsSortOn.equals(primKey.getDomains().get(0))) {
                                switch (primKey.getIndexType()) {
                                    case BPlusTree:
                                        costRead = (primKey.getIndexAttributes().getHeight() + 1)
                                                *
                                                (catalog.getDiskAccessLatencyMillisecs()
                                                        +
                                                        catalog.getDiskTransferTimeMillisecs());
                                        costWriteFinal = catalog.getDiskTransferTimeMillisecs()
                                                            + catalog.getDiskAccessLatencyMillisecs();
                                        break;
                                    case Hash:
                                        break;
                                    case ISAM:
                                        break;
                                }
                            }
                        default: //everything other than NOT_EQUAL_TO
                            if (requestsSortOn.equals(primKey.getDomains().get(0))) {
                                switch (primKey.getIndexType()) {
                                    case BPlusTree:
                                        //cost = indexAccess + cost of accessing what
                                        //       ends up to be the result size (estimated)
                                        setCostRead(primKey.getIndexAttributes().getHeight()
                                                *
                                                (catalog.getDiskAccessLatencyMillisecs()
                                                        +
                                                        catalog.getDiskTransferTimeMillisecs()) + getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                                        setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                                        break;
                                    case Hash:
                                        break;
                                    case ISAM:
                                        break;
                                }
                            }
                    }
                }
                switch (comp) {
                    case EQUALS:
                        setReadAlgorithm(AccessTypeEnum.PRIMARY_INDEX_KNIFE);
                        break;
                    case GREATER_THAN:
                    case GREATER_OR_EQUAL:
                        setReadAlgorithm(AccessTypeEnum.PRIMARY_INDEX_SCAN);
                        break;
                    case LESS_OR_EQUAL:
                    case LESS_THAN:
                        setReadAlgorithm(AccessTypeEnum.DESCEND_INDEX_SCAN);
                }
            } else {
                setCostRead(expr.getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                catalog.getDiskAccessLatencyMillisecs();
            }
        } else if (getRequestsSortOn() == null) {
            setReadAlgorithm(LINEAR_SCAN);
            setCostRead(expr.getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
            setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
            catalog.getDiskAccessLatencyMillisecs();
        } else { //input sorted on something, but no relation data
            setReadAlgorithm(LINEAR_SCAN);
            switch (comp) {
                case LESS_THAN:
                case LESS_OR_EQUAL:
                case EQUALS:
                    setCostRead(expr.getSizeBlocks() / 2 * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                    setCostWriteFinal(getCostRead() - catalog.getDiskAccessLatencyMillisecs());
                    break;
                case GREATER_THAN:
                case GREATER_OR_EQUAL:
                    setCostRead(expr.getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                    setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
                    catalog.getDiskAccessLatencyMillisecs();
                    break;
            }
        }
        if (!expr.externalMergeSortResults && requestsSortOn != null) { //if other doesn't have to sort results, check pipelineability
            if (requestsSortOn.equals(expr.resultSortedOn) && !(expr instanceof RelationNode)) {
                pipelineable = true;
            }
        }
        return getCostWriteFinal();
    }

    public void addJoinNodeParentsDFS(Map<TreeNode, List<Condition>> parentList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        this.expr.getJoinNodeCluster(commonConditions, joinNodeCluster);
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {
        this.expr.fillMultiClusterMap(multiClusterMap);
    }
}