package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

public interface Operand {
    public Integer getAsNumericalValue();

    public String getAttributeName();

    public String getRelationName();
}
