/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

/**
 * @author anantoni
 */
public class StringLiteral implements Operand {
    private final String textRepr;

    public StringLiteral(String literal) {
        this.textRepr = literal.replace("\"", "");
    }

    /**
     * @return the literal
     */
    public String getLiteral() {
        return textRepr;
    }

    @Override
    public Integer getAsNumericalValue() {
        int result = textRepr.length() & 0x7f;    // get 7 bits.
        char x[] = textRepr.toCharArray();
        int i;

        i = (x.length < 3)
                ? x.length
                : 3;

        for (int j = 0; j < i; j++) {
            result <<= 8;
            result |= x[j] & 0xff;                 // ensure one-byte character.
        }

        while (i++ < 3) {
            result <<= 8;
        }

        return result;
    }

    @Override
    public String getAttributeName() {
        return null;
    }

    @Override
    public String getRelationName() {
        return null;
    }

    @Override
    public String toString() {
        return "\\\"" + textRepr + "\\\"";
    }
}
