/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public abstract class TreeNode {
    private static int ID = 0;
    static private int graphID;
    protected int dotNotationId;
    protected Set<TreeNode> parents;
    protected Set<Attribute> attributes;
    protected Integer numRecords;
    protected Integer sizeBlocks;
    protected Integer costRead;
    protected Integer costIntermediate = 0;
    protected Integer costWriteFinal;
    protected AccessTypeEnum readAlgorithm;
    protected Attribute resultSortedOn;
    protected Attribute requestsSortOn;
    protected Attribute requestsSortOnRight;
    protected boolean externalMergeSortResults;
    protected boolean fullySortedResults;
    protected boolean pipelineable;

    public TreeNode(TreeNode other) {
        dotNotationId = other.dotNotationId;
        parents = other.parents;
        attributes = other.attributes;
        numRecords = other.numRecords;
        sizeBlocks = other.sizeBlocks;
//        costRead = other.costRead;
//        costIntermediate = other.costIntermediate;
//        costWriteFinal = other.costWriteFinal;
        readAlgorithm = other.readAlgorithm;
        resultSortedOn = other.resultSortedOn;
        requestsSortOn = other. requestsSortOn;
        requestsSortOnRight = other. requestsSortOnRight;
        externalMergeSortResults = other.externalMergeSortResults;
        pipelineable = other.pipelineable;
        fullySortedResults = other.fullySortedResults;
    }
    
    
    public Integer getCostIntermediate() {
        return costIntermediate;
    }

    public void setCostIntermediate(Integer costIntermediate) {
        this.costIntermediate = costIntermediate;
    }

    public boolean isPipelineable() {
        return pipelineable;
    }

    public void setPipelineable(boolean pipelineable) {
        this.pipelineable = pipelineable;
    }

    public int getMemoryBufferAmountCostSoFar() {
        return memoryBufferAmountCostSoFar;
    }

    public void setMemoryBufferAmountCostSoFar(int memoryBufferAmountCostSoFar) {
        this.memoryBufferAmountCostSoFar = memoryBufferAmountCostSoFar;
    }
    protected int memoryBufferAmountCostSoFar;

    TreeNode() {
        parents = new HashSet<>();
        dotNotationId = ID++;
        numRecords = null;    // for emphasis
        costRead = null;    // for emphasis
        costIntermediate = 0;
        resultSortedOn = null;
        requestsSortOn = null;
        requestsSortOnRight = null;
        fullySortedResults = false;
    }

    /**
     * @return the ID
     */
    public static int getID() {
        return ID;
    }

    /**
     * @param aID the ID to set
     */
    public static void setID(int aID) {
        ID = aID;
    }

    /**
     * @return the graphID
     */
    public static int getGraphID() {
        return graphID;
    }

    /**
     * @param aGraphID the graphID to set
     */
    public static void setGraphID(int aGraphID) {
        graphID = aGraphID;
    }

    protected void printDotNotationEdge(TreeNode other, PrintWriter pw) {

        // pw.println("a" + other.dotNotationId + "->a" + dotNotationId + " [color = \"brown\"]");
        pw.print("a" + getGraphID() + getDotNotationId() + "->a" + getGraphID() + other.getDotNotationId() + " [color = \"brown\" dir=\"none\" label=\"");
        if (other.sizeBlocks != null) {
            pw.print("Size(blocks): " + other.sizeBlocks);
        } else if (other.getNumRecords() != null)
            pw.print("Size(recs): " + other.getNumRecords());
        if (!pipelineable && !(other instanceof RelationNode) && other.costWriteFinal != null)
            pw.print("\\nWrite Cost: " + other.costWriteFinal + "ms");
        if (other.externalMergeSortResults) {
            pw.print("\\n--External Merge Sort--");
        }
        pw.println("\"]");
    }

    public abstract Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException;

    public abstract int annotateSize() throws SyntaxErrorException;

    public abstract void commandLinePrint();

    public abstract void printNodeInfo();

    public abstract void selectionSplit();    // Break complex conjuction selects

    public abstract void addListNodeParentsDFS(Class childType, List<TreeNode> parentList);

    public abstract void selectionPushdownInit();

    public abstract void selectionPushdown(TreeNode selectionNode);    // Push selections to the leaves of the query tree

    public abstract void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster);

    public abstract void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap);

    /**
     * pushDown
     *
     * @param node
     * @return self upon success, null upon failure, other upon split.
     */
    public abstract SelectionNode pushDown(SelectionNode node);

    public abstract ProjectionNode pushDown(ProjectionNode node);

    /**
     * @param treeNode
     */
    public void setExpr(TreeNode treeNode) {
    }

    public TreeNode getExpr() {
        return null;
    }

    public abstract Set<TreeNode> getExprAsSet();

    public abstract Set<Relation> getRelations();

    public void printDotNotation(PrintWriter pw, int graphID) {
        TreeNode.setGraphID(graphID);
        printDotNotation(pw);
    }

    protected void printDotNotation(PrintWriter pw) {
        pw.print("a" + getGraphID() + getDotNotationId() + " ");
    }

    public Set<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(Set<Attribute> attributes) {
        this.attributes = attributes;
    }

    public abstract void changeChild(TreeNode oldChild, TreeNode newChild);

    public Integer getSize() {
        return getNumRecords();
    }

    public Integer getCostRead() {
        if (costRead == null)
            return null;
        return pipelineable ? costRead : 0;
    }

    /**
     * @param cost the cost to set
     */
    public void setCostRead(Integer cost) {
        this.costRead = cost;
    }

    public boolean hasChainedSuccessor(Class cl, TreeNode instance) {
        if (instance.getClass().equals(cl)) {
            Set<TreeNode> successors = instance.getExprAsSet();
            for (TreeNode successor : successors) {
                if (instance.equals(successor))
                    return true;
            }
            for (TreeNode successor : successors) {
                if (successor.hasChainedSuccessor(cl, instance))
                    return true;
            }
        }
        return false;
    }

    public abstract Integer annotateCost(Catalog catalog);

    public void normalizeSizeBlocks(Catalog cat) throws SyntaxErrorException {
        if (getNumRecords() == null) {
            annotateSize();
        }
        for (TreeNode tn : getExprAsSet()) {
            tn.normalizeSizeBlocks(cat);
        }
        int recordSize = 0;
        for (Attribute attr : getAttributes()) {
            recordSize += attr.getSize();
        }
        setSizeBlocks(getNumRecords() * recordSize / cat.getDiskPageSizeBytes() + (((getNumRecords() * recordSize) % cat.getDiskPageSizeBytes() == 0) ? 0 : 1));
    }

    public void normalizeSizeBlocksNoRecursion(Catalog cat) throws SyntaxErrorException {
        int recordSize = 0;
        for (Attribute attr : getAttributes()) {
            recordSize += attr.getSize();
        }
        setSizeBlocks(getNumRecords() * recordSize / cat.getDiskPageSizeBytes() + (((getNumRecords() * recordSize) % cat.getDiskPageSizeBytes() == 0) ? 0 : 1));
    }

    public Attribute getResultSortedOn() {
        return resultSortedOn;
    }

    /**
     * @param resultSortedOn the resultSortedOn to set
     */
    public void setResultSortedOn(Attribute resultSortedOn) {
        this.resultSortedOn = resultSortedOn;
    }

    /**
     * @return the dotNotationId
     */
    public int getDotNotationId() {
        return dotNotationId;
    }

    /**
     * @param dotNotationId the dotNotationId to set
     */
    public void setDotNotationId(int dotNotationId) {
        this.dotNotationId = dotNotationId;
    }

    /**
     * @return the parents
     */
    public Set<TreeNode> getParents() {
        return parents;
    }

    /**
     * @param parents the parents to set
     */
    public void setParents(Set<TreeNode> parents) {
        this.parents = parents;
    }

    /**
     * @return the numRecords
     */
    public Integer getNumRecords() {
        return numRecords;
    }

    /**
     * @param numRecords the numRecords to set
     */
    public void setNumRecords(Integer numRecords) {
        this.numRecords = numRecords;
    }

    /**
     * @return the sizeBlocks
     */
    public Integer getSizeBlocks() {
        return sizeBlocks;
    }

    /**
     * @param sizeBlocks the sizeBlocks to set
     */
    public void setSizeBlocks(Integer sizeBlocks) {
        this.sizeBlocks = sizeBlocks;
    }

    /**
     * @return the costWriteFinal
     */
    public Integer getCostWriteFinal() {
        return costWriteFinal;
    }

    /**
     * @param costWriteFinal the costWriteFinal to set
     */
    public void setCostWriteFinal(Integer costWriteFinal) {
        this.costWriteFinal = costWriteFinal;
    }

    /**
     * @return the readAlgorithm
     */
    public AccessTypeEnum getReadAlgorithm() {
        return readAlgorithm;
    }

    /**
     * @param readAlgorithm the readAlgorithm to set
     */
    public void setReadAlgorithm(AccessTypeEnum readAlgorithm) {
        this.readAlgorithm = readAlgorithm;
    }

    /**
     * @return the requestsSortOn
     */
    public Attribute getRequestsSortOn() {
        return requestsSortOn;
    }

    /**
     * @param requestsSortOn the requestsSortOn to set
     */
    public void setRequestsSortOn(Attribute requestsSortOn) {
        this.requestsSortOn = requestsSortOn;
    }

    /**
     * @return the getRequestsSortOnRight
     */
    public Attribute getRequestsSortOnRight() {
        return requestsSortOnRight;
    }

    /**
     * @param requestsSortOn the requestsSortOn2 to set
     */
    public void setRequestsSortOnRight(Attribute requestsSortOn) {
        this.requestsSortOnRight = requestsSortOn;
    }

    public boolean isExternalMergeSortResults() {
        return externalMergeSortResults;
    }

    public void setExternalMergeSortResults(boolean externalMergeSortResults) {
        this.externalMergeSortResults = externalMergeSortResults;
    }
}
