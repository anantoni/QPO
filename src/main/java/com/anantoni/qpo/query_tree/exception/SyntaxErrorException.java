package com.anantoni.qpo.query_tree.exception;

public class SyntaxErrorException extends Exception {
    public SyntaxErrorException(String message) {
        super(message);
    }
}
