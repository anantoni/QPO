package com.anantoni.qpo.query_tree.exception;

public class UnknownRelationException extends Exception {
    public UnknownRelationException(String message) {
        super(message);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
