/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

/**
 * @author anantoni
 */
public enum JoinTypeEnum {
    NONE, HASH_JOIN, MERGE_JOIN, BLOCK_NESTED_LOOP_JOIN
}
