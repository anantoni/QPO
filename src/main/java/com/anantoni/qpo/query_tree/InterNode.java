/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.optimizer.Optimizer;
import static com.anantoni.qpo.query_tree.AccessTypeEnum.LINEAR_SCAN;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class InterNode extends TreeNode {
    private TreeNode leftExpr;
    private TreeNode rightExpr;
    private Set<Relation> relations;

    public InterNode(TreeNode leftExpr, TreeNode rightExpr) {
        this.leftExpr = leftExpr;
        this.rightExpr = rightExpr;
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        if (father != null) {
            this.getParents().add(father);
        }

        Set<Relation> temp1 = leftExpr.referenceBuildPass(catalog, this);
        Set<Relation> temp2 = rightExpr.referenceBuildPass(catalog, this);

        relations = new HashSet<>();
        relations.addAll(temp1);
        relations.addAll(temp2);

        return relations;
    }

    @Override
    public void commandLinePrint() {
        printNodeInfo();
        leftExpr.commandLinePrint();
        rightExpr.commandLinePrint();
    }

    @Override
    public void printNodeInfo() {
        System.out.println("InterNode");
    }

    @Override
    public void printDotNotation(PrintWriter pw) {
        super.printDotNotation(pw);
        pw.print("[label=\"∩");
        if (resultSortedOn != null) {
            pw.print("\\nSorted On: " + resultSortedOn.getName());
        }
        if (requestsSortOn != null) {
            pw.print("\\nReqSort: " + requestsSortOn.getName());
        }
        if (requestsSortOnRight != null) {
            pw.print("\\nReqSortR: " + requestsSortOnRight.getName());
        }
        pw.println("\" shape=box]");
        printDotNotationEdge(leftExpr, pw);
        printDotNotationEdge(rightExpr, pw);
        leftExpr.printDotNotation(pw);
        rightExpr.printDotNotation(pw);
    }

    @Override
    public void selectionSplit() {
        this.leftExpr.selectionSplit();
        this.rightExpr.selectionSplit();
    }

    @Override
    public Set<Relation> getRelations() {
        return relations;
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
        if (this.leftExpr.getClass().equals(childType) || this.rightExpr.getClass().equals(childType)) {
            parentList.add(this);
        }

        this.leftExpr.addListNodeParentsDFS(childType, parentList);
        this.rightExpr.addListNodeParentsDFS(childType, parentList);
    }

    @Override
    public void selectionPushdownInit() {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @param selectionNode
     */
    @Override
    public void selectionPushdown(TreeNode selectionNode) {
        Operand attribute = ((SelectionNode) selectionNode).getConditionList().get(0).getFstOperand();
        String attrName = ((AttributeRef) attribute).getAttrName();
        Set<Relation> leftSet = this.leftExpr.getRelations();
        Set<Relation> rightSet = this.rightExpr.getRelations();

        for (Relation rel : leftSet) {
            if (rel.hasAttribute(attrName)) {
                if ((this.leftExpr instanceof RelationNode) || (this.leftExpr instanceof SelectionNode)) {
                    TreeNode temp = this.leftExpr;

                    this.leftExpr = selectionNode;
                    selectionNode.setExpr(temp);
                } else {
                    leftExpr.selectionPushdown(selectionNode);
                }

                break;
            }
        }

//        for (Relation rel : rightSet) {
//            System.out.println(rel);
//            System.out.println(rel.getRelationName());
//        }

        SelectionNode selectionNodeCopy = new SelectionNode((SelectionNode) selectionNode);

        for (Relation rel : rightSet) {
            if (rel.hasAttribute(attrName)) {
                if ((this.rightExpr instanceof RelationNode) || (this.rightExpr instanceof SelectionNode)) {
                    TreeNode temp = this.rightExpr;

                    this.rightExpr = selectionNodeCopy;
                    selectionNodeCopy.setExpr(temp);
                } else {
                    this.rightExpr.selectionPushdown(selectionNodeCopy);
                }

                break;
            }
        }
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        Set<TreeNode> retVal = new HashSet<>();

        retVal.add(leftExpr);
        retVal.add(rightExpr);

        return retVal;
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {
        if (leftExpr == oldChild) {
            leftExpr = newChild;
        } else if (rightExpr == oldChild) {
            rightExpr = newChild;
        } else {
            System.err.println("Unrelated child");
        }
    }

    @Override
    public int annotateSize() throws SyntaxErrorException {
        int leftSize = leftExpr.annotateSize();
        int rightSize = rightExpr.annotateSize();

        Set<Attribute> leftAttrSet = leftExpr.getAttributes();
        Set<Attribute> rightAttrSet = rightExpr.getAttributes();

        setAttributes(new HashSet<>());

        if (!leftAttrSet.equals(rightAttrSet)) {
            throw new SyntaxErrorException("Intersection Attributes not equal");
        }

        for (Attribute lAttr : leftAttrSet) {
            for (Attribute rAttr : rightAttrSet) {
                if (lAttr.equals(rAttr)) {
                    Attribute myAttr = new Attribute(lAttr.getName(), lAttr.getType(),
                            lAttr.getSize(), null, null, null, null);

                    Integer lUnique = lAttr.getUniqueValues(),
                            rUnique = rAttr.getUniqueValues();

                    if (lUnique != null && rUnique != null) {
                        if (lUnique > rUnique)
                            myAttr.setUniqueValues(rUnique);
                        else
                            myAttr.setUniqueValues(lUnique);
                    }

                    if (lAttr.getMaxValue() < rAttr.getMaxValue()) {
                        myAttr.setMaxValue(lAttr.getMaxValue());
                    } else {
                        myAttr.setMaxValue(rAttr.getMaxValue());
                    }

                    if (lAttr.getMinValue() > rAttr.getMinValue()) {
                        myAttr.setMinValue(lAttr.getMinValue());
                    } else {
                        myAttr.setMinValue(rAttr.getMinValue());
                    }

                    getAttributes().add(myAttr);

                    break;
                }
            }
        }


        return numRecords = (leftSize < rightSize)
                ? leftSize
                : rightSize;
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        leftExpr.annotateCost(catalog);
        rightExpr.annotateCost(catalog);
        fullySortedResults = true;
        //hash sort results previously
        int m = catalog.getMemoryAmountOfBuffers();
        int br = leftExpr.sizeBlocks;
        int bs = rightExpr.sizeBlocks;
        int nextIntlogR = Optimizer.nextIntegerlog(m - 1, br / m);
        int nextIntlogS = Optimizer.nextIntegerlog(m - 1, bs / m);
        setReadAlgorithm(LINEAR_SCAN);
        setCostRead((Integer) (leftExpr.getSizeBlocks() + rightExpr.getSizeBlocks()) * catalog.getDiskTransferTimeMillisecs() + catalog.getDiskAccessLatencyMillisecs());
        setCostWriteFinal(getSizeBlocks() * catalog.getDiskTransferTimeMillisecs() * 2 + catalog.getDiskAccessLatencyMillisecs());
        catalog.getDiskAccessLatencyMillisecs();
        if (leftExpr.externalMergeSortResults == false) {
            leftExpr.externalMergeSortResults = true;
            costIntermediate += br * (2 * nextIntlogR + 1) * catalog.getDiskTransferTimeMillisecs()
                    + (2 * (br / m + (br % m == 0 ? 0 : 1))
                    + (2 * nextIntlogR - 1)) * catalog.getDiskAccessLatencyMillisecs();
        }
        resultSortedOn = leftExpr.getResultSortedOn();
        if (rightExpr.externalMergeSortResults == false) {
            rightExpr.externalMergeSortResults = true;
            costIntermediate += bs * (2 * nextIntlogS + 1) * catalog.getDiskTransferTimeMillisecs()
                    + (2 * (bs / m + (bs % m == 0 ? 0 : 1))
                    + (2 * nextIntlogS - 1)) * catalog.getDiskAccessLatencyMillisecs();
            catalog.getDiskAccessLatencyMillisecs();
        }
        return costWriteFinal;
    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {
        if (this.leftExpr instanceof JoinNode)
            this.leftExpr.fillMultiClusterMap(multiClusterMap);
        if (this.rightExpr instanceof JoinNode)
            this.rightExpr.fillMultiClusterMap(multiClusterMap);
    }
}
