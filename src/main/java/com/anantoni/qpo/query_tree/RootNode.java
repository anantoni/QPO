package com.anantoni.qpo.query_tree;

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by anantoni on 16/3/2015.
 */
public class RootNode extends TreeNode {
    private TreeNode expr;

    public RootNode(TreeNode treeNode) {
        setExpr(treeNode);
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        return expr.referenceBuildPass(catalog, father);
    }

    @Override
    public int annotateSize() throws SyntaxErrorException {
        return expr.annotateSize();
    }

    @Override
    public void commandLinePrint() {
        expr.commandLinePrint();
    }

    @Override
    public void printNodeInfo() {
        expr.printNodeInfo();
    }

    @Override
    public void selectionSplit() {
        expr.selectionSplit();
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
        if (this.getExpr().getClass().equals(childType)) {
            parentList.add(this);
        }

        this.getExpr().addListNodeParentsDFS(childType, parentList);
    }

    @Override
    public void selectionPushdownInit() {
        if (this.expr instanceof SelectionNode) {
            TreeNode selectionNode = this.getExpr();
            TreeNode childNode = this.getExpr().getExpr();

            this.setExpr(childNode);
            this.getExpr().selectionPushdown(selectionNode);
        }

    }

    @Override
    public void selectionPushdown(TreeNode selectionNode) {

    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        this.expr.getJoinNodeCluster(commonConditions, joinNodeCluster);
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {
        this.expr.fillMultiClusterMap(multiClusterMap);
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        return null;
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        return null;
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        return this.expr.getExprAsSet();
    }

    @Override
    public Set<Relation> getRelations() {
        return this.expr.getRelations();
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {

    }


    @Override
    protected void printDotNotationEdge(TreeNode other, PrintWriter pw) {
        pw.print("a" + getGraphID() + getDotNotationId() + "->a" + getGraphID() + other.getDotNotationId() + " [color = \"brown\" dir=\"none\" ");
        if (other.externalMergeSortResults) {
            pw.print("label=\"--External Merge-Sort--\"");
        }
        pw.print("]\n");
    }

    @Override
    protected void printDotNotation(PrintWriter pw) {
        super.printDotNotation(pw);
        pw.println("[shape=none label=\"RESULT\"]");
        printDotNotationEdge(expr, pw);
        expr.printDotNotation(pw);
    }

    @Override
    public boolean hasChainedSuccessor(Class cl, TreeNode instance) {
        return super.hasChainedSuccessor(cl, instance);
    }

    @Override
    public void normalizeSizeBlocksNoRecursion(Catalog cat) throws SyntaxErrorException {
        this.expr.normalizeSizeBlocksNoRecursion(cat);
    }

    @Override
    public Attribute getResultSortedOn() {
        return super.getResultSortedOn();
    }

    @Override
    public void setResultSortedOn(Attribute resultSortedOn) {
        super.setResultSortedOn(resultSortedOn);
    }

    @Override
    public int getDotNotationId() {
        return super.getDotNotationId();
    }

    @Override
    public void setDotNotationId(int dotNotationId) {
        super.setDotNotationId(dotNotationId);
    }

    @Override
    public Set<TreeNode> getParents() {
        return super.getParents();
    }

    @Override
    public void setParents(Set<TreeNode> parents) {
        super.setParents(parents);
    }

    @Override
    public void setAttributes(Set<Attribute> attributes) {
        super.setAttributes(attributes);
    }

    @Override
    public Integer getNumRecords() {
        return super.getNumRecords();
    }

    @Override
    public void setNumRecords(Integer numRecords) {
        super.setNumRecords(numRecords);
    }

    @Override
    public Integer getSizeBlocks() {
        return super.getSizeBlocks();
    }

    @Override
    public void setSizeBlocks(Integer sizeBlocks) {
        super.setSizeBlocks(sizeBlocks);
    }

    @Override
    public void setCostRead(Integer cost) {
        super.setCostRead(cost);
    }

    @Override
    public Integer getCostWriteFinal() {
        return super.getCostWriteFinal();
    }

    @Override
    public void setCostWriteFinal(Integer costWriteFinal) {
        super.setCostWriteFinal(costWriteFinal);
    }

    @Override
    public AccessTypeEnum getReadAlgorithm() {
        return super.getReadAlgorithm();
    }

    @Override
    public void setReadAlgorithm(AccessTypeEnum readAlgorithm) {
        super.setReadAlgorithm(readAlgorithm);
    }

    @Override
    public Attribute getRequestsSortOn() {
        return super.getRequestsSortOn();
    }

    @Override
    public void setRequestsSortOn(Attribute requestsSortOn) {
        super.setRequestsSortOn(requestsSortOn);
    }

    @Override
    public Attribute getRequestsSortOnRight() {
        return super.getRequestsSortOnRight();
    }

    @Override
    public void setRequestsSortOnRight(Attribute requestsSortOn2) {
        super.setRequestsSortOnRight(requestsSortOn2);
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        return this.expr.annotateCost(catalog);
    }

    public TreeNode getExpr() {
        return expr;
    }

    @Override
    public void setExpr(TreeNode expr) {
        this.expr = expr;
    }

    public void normalizeSizeBlocks(Catalog cat) throws SyntaxErrorException {
        this.expr.normalizeSizeBlocks(cat);
    }
}
