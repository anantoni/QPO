/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Attribute;
import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.query_tree.exception.SyntaxErrorException;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.*;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class JoinNode extends TreeNode {
    private final List<Condition> conditionList;
    private final JunctionEnum junctionType;
    Set<Relation> relations;
    private TreeNode outerRelation;
    private TreeNode innerRelation;
    private JoinTypeEnum joinType;
    private boolean innerFitsMemory;
    private boolean outerFitsMemory;
    private boolean pipelineableInner;

    public JoinNode() {
        joinType = JoinTypeEnum.NONE;
        this.innerFitsMemory = false;
        this.conditionList = null;
        this.junctionType = null;
        super.costRead = 0;
    }
    
    public JoinNode(JoinNode other) {
        super(other);
        conditionList = other.conditionList;
        junctionType = other.junctionType;
        relations = other.relations;
        outerRelation = other.outerRelation;
        innerRelation = other.innerRelation;
        joinType = other.joinType;
        innerFitsMemory = other.innerFitsMemory;
        pipelineableInner = other.pipelineableInner;
    }

    public JoinNode(TreeNode leftRelation, TreeNode rightRelation, List<Condition> conditionList, JunctionEnum junctionType) {
        joinType = JoinTypeEnum.NONE;
        this.innerFitsMemory = false;
        this.conditionList = conditionList;
        this.outerRelation = leftRelation;
        this.innerRelation = rightRelation;
        this.junctionType = junctionType;
    }

    @SuppressWarnings("empty-statement")
    public JoinNode(List<String> conditionList, TreeNode leftRelation, TreeNode rightRelation) {
        joinType = JoinTypeEnum.NONE;
        this.innerFitsMemory = false;
        this.outerRelation = leftRelation;
        this.innerRelation = rightRelation;
        this.conditionList = new ArrayList<>();

        String junction = "";

        /**
         * Input condition list format is op11, comp, op12 (, {AND|OR}, op21, comp, op22)
         * We need to identify junction type
         */
        if (conditionList.size() > 3) {
            junction = conditionList.get(3);
        }

        if (junction.equals("AND")) {
            this.junctionType = JunctionEnum.AND;
        } else {
            this.junctionType = JunctionEnum.OR;
        }

        // Remove junction strings from list
        while (conditionList.remove(junction)) ;

        ListIterator<String> li = conditionList.listIterator();

        while (li.hasNext()) {
            String firstOperand = li.next();
            String comp = li.next();
            String secondOperand = li.next();
            Operand sndOperand;

            if (secondOperand.contains("\"")) {
                sndOperand = new StringLiteral(secondOperand);
            } else if (IntegerLiteral.isInteger(secondOperand)) {
                sndOperand = new IntegerLiteral(secondOperand);
            } else {
                sndOperand = new AttributeRef(secondOperand);
            }

            this.conditionList.add(new Condition(new AttributeRef(firstOperand), comp, sndOperand));
        }
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        if (father != null) {
            this.getParents().add(father);
        }

        Set<Relation> temp2 = outerRelation.referenceBuildPass(catalog, this);
        Set<Relation> temp1 = innerRelation.referenceBuildPass(catalog, this);

        relations = new HashSet<>();
        relations.addAll(temp1);
        relations.addAll(temp2);

        return relations;
    }

    @Override
    public void commandLinePrint() {
        printNodeInfo();
        outerRelation.commandLinePrint();
        innerRelation.commandLinePrint();
    }

    @Override
    public void printNodeInfo() {
        System.out.print("JoinNode");

        for (Condition condition : conditionList) {
            System.out.println(condition.toString());
        }

        System.out.print(" ");

        if (junctionType == JunctionEnum.AND) {
            System.out.println("Conjunction");
        } else {
            System.out.println("Disjunction");
        }
    }

    @Override
    public void printDotNotation(PrintWriter pw) {
        char junction;

        super.printDotNotation(pw);
        pw.print("[label=\"⨝(");

        if (junctionType == JunctionEnum.AND) {
            junction = '∧';
        } else if (junctionType == JunctionEnum.OR) {
            junction = '∨';
        } else {
            junction = ' ';
        }

        ListIterator<Condition> li = conditionList.listIterator();

        while (li.hasNext()) {
            if (li.nextIndex() != (conditionList.size() - 1)) {
                pw.print(li.next().toString() + junction);
            } else {
                pw.print(li.next().toString());
            }
        }

        pw.print(")");
        switch (joinType) {
            case BLOCK_NESTED_LOOP_JOIN:
                pw.print("\\n--BLOCK NESTED LOOP JOIN--");
                break;
            case HASH_JOIN:
                pw.print("\\n--HASH JOIN--");
                break;
            case MERGE_JOIN:
                pw.print("\\n--MERGE JOIN--");
                break;
        }
        if (costRead != null) {
            pw.print("\\nCost: " + (costRead + costIntermediate) + "ms");
        }
        pw.print("\" shape=box]");
        printDotNotationEdge(outerRelation, pw);
        printDotNotationEdge(innerRelation, pw);
        outerRelation.printDotNotation(pw);
        innerRelation.printDotNotation(pw);
    }

    @Override
    protected void printDotNotationEdge(TreeNode other, PrintWriter pw) {
        pw.print("a" + getGraphID() + getDotNotationId() + "->a"
                 + getGraphID() + other.getDotNotationId()
                 + " [color = \"brown\" dir=\"none\" label=\"");
        if (other.sizeBlocks != null) {
            pw.print("Size(blocks): " + other.sizeBlocks);
        }
        else if (other.numRecords != null) {
            pw.print("Size(recs): " + other.numRecords);
        }
        if (other.externalMergeSortResults) {
            pw.print("\\n--External Merge-Sort--");
        }
        if (requestsSortOn != null) {
            if (other == innerRelation) {
                if (pipelineableInner) {
                    pw.print("\\n--Pipelined--");
                }
                if (innerFitsMemory) {
                    pw.print("\\n--Inner fits Memory--");
                }
                else if (!(pipelineable || innerFitsMemory) && other.costWriteFinal != null)
                    pw.print("\\nWrite Cost: " + innerRelation.costWriteFinal + "ms");
            }
            else if (requestsSortOn.equals(outerRelation.resultSortedOn)) {
                pw.print("\\n--Pipelined--");
            }
            else if (other.costWriteFinal != null)
                pw.print("\\nWrite Cost: " + outerRelation.costWriteFinal + "ms");
        }
        pw.println("\"]");
    }
    
    @Override
    public Integer getCostRead() {
        return costRead;
    }
    
    public Integer getTotalCost() {
        return costRead
                + costIntermediate
                + ((pipelineableInner || innerFitsMemory) ? 0 : (innerRelation.costWriteFinal == null ? 0 : innerRelation.costWriteFinal))
                + (pipelineable ? 0 : (outerRelation.costWriteFinal == null ? 0 : outerRelation.costWriteFinal));
    }
    
    public Integer getSimpleCost() {
        return costRead;
    }

    @Override
    public void selectionSplit() {
        this.outerRelation.selectionSplit();
        this.innerRelation.selectionSplit();
    }

    @Override
    public Set<Relation> getRelations() {
        return relations;
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
        if (this.outerRelation.getClass().equals(childType) || this.innerRelation.getClass().equals(childType)) {
            parentList.add(this);
        }

        this.outerRelation.addListNodeParentsDFS(childType, parentList);
        this.innerRelation.addListNodeParentsDFS(childType, parentList);
    }

    /**
     *
     */
    @Override
    public void selectionPushdownInit() {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectionPushdown(TreeNode selectionNode) {
        Operand attribute = ((SelectionNode) selectionNode).getConditionList().get(0).getFstOperand();
        String attrName = ((AttributeRef) attribute).getAttrName();
        Set<Relation> outerSet = outerRelation.getRelations();
        Set<Relation> innerSet = innerRelation.getRelations();
        boolean outerFound = false;

        for (Relation rel : outerSet) {
            if (rel.hasAttribute(attrName)) {

                if ((outerRelation instanceof RelationNode) || (outerRelation instanceof SelectionNode)) {
                    TreeNode temp = this.outerRelation;

                    this.outerRelation = selectionNode;
                    selectionNode.setExpr(temp);
                } else {
                    outerRelation.selectionPushdown(selectionNode);
                }
                outerFound = true;
                break;
            }
        }

        SelectionNode selectionNodeCopy = new SelectionNode((SelectionNode) selectionNode);

        for (Relation rel : innerSet) {
            if (rel.hasAttribute(attrName)) {

                if ((this.innerRelation instanceof RelationNode) || (this.innerRelation instanceof SelectionNode)) {

                    if (!outerFound) {
                        TreeNode temp = this.innerRelation;
                        this.innerRelation = selectionNode;
                        selectionNode.setExpr(temp);
                    } else {
                        TreeNode temp = this.innerRelation;
                        this.innerRelation = selectionNodeCopy;
                        selectionNodeCopy.setExpr(temp);
                    }

                } else {
                    if (outerFound == true)
                        innerRelation.selectionPushdown(new SelectionNode((SelectionNode) selectionNode));
                    else
                        innerRelation.selectionPushdown(selectionNode);
                }
                break;
            }
        }
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        Set<TreeNode> retVal = new HashSet<>();

        retVal.add(innerRelation);
        retVal.add(outerRelation);

        return retVal;
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {
        if (innerRelation == oldChild) {
            innerRelation = newChild;
        } else if (outerRelation == oldChild) {
            outerRelation = newChild;
        } else {
            System.err.println("Unrelated child");
        }
    }

    public int annotateSizeNoRecursion() throws SyntaxErrorException {
        Integer uniqueValsInner = 0,
                uniqueValsOuter = 0;

        Set<Attribute> leftAttrSet = innerRelation.getAttributes();
        Set<Attribute> rightAttrSet = outerRelation.getAttributes();

        setAttributes(new HashSet<>());
        for (Attribute attr : leftAttrSet)
            getAttributes().add(new Attribute(attr));
        for (Attribute attr : rightAttrSet)
            getAttributes().add(new Attribute(attr));

        for (Condition cond : conditionList) {

            cond.attributeBuildPass(outerRelation.getAttributes(), innerRelation.getAttributes());
        }

        for (Attribute attr : innerRelation.getAttributes()) {
            Integer uv = attr.getUniqueValues();

            if (uv == null) {
                uniqueValsInner = innerRelation.getSize();
                break;
            }

            uniqueValsInner = (uniqueValsInner < uv)
                    ? uv
                    : uniqueValsInner;
        }

        for (Attribute attr : outerRelation.getAttributes()) {
            Integer uv = attr.getUniqueValues();

            if (uv == null) {
                uniqueValsOuter = outerRelation.getSize();

                break;
            }

            uniqueValsOuter = (uniqueValsOuter < uv)
                    ? uv
                    : uniqueValsOuter;
        }

        int uniqueVals = (uniqueValsInner > uniqueValsOuter)
                ? uniqueValsInner
                : uniqueValsOuter;

        setNumRecords((int) Math.ceil(innerRelation.getSize() * outerRelation.getSize() / (double) uniqueVals));
        for (Attribute attr : attributes) {
            int caseCtr = 0;
            Integer innerAttrUnique = null, outerAttrUnique = null;
            for (Attribute attrInner : innerRelation.getAttributes()) {
                if (attrInner.equals(attr)) {
                    caseCtr += 1;
                    innerAttrUnique = attrInner.getUniqueValues();
                    break;
                }
            }
            for (Attribute attrOuter : outerRelation.getAttributes()) {
                if (attrOuter.equals(attr)) {
                    caseCtr += 1;
                    outerAttrUnique = attrOuter.getUniqueValues();
                    break;
                }
            }
            switch (caseCtr) {
                case 2:
                    if (innerAttrUnique != null) {
                        if (outerAttrUnique != null) {
                            if (innerAttrUnique < outerAttrUnique)
                                attr.setUniqueValues(innerAttrUnique);
                            else
                                attr.setUniqueValues(outerAttrUnique);
                        } else {
                            attr.setUniqueValues(innerAttrUnique);
                        }
                    } else
                        attr.setUniqueValues(outerAttrUnique);
                    //purposely drop to case 1 to check for size & null.
                case 1:
                    if (attr.getUniqueValues() == null || getNumRecords() < attr.getUniqueValues())
                        attr.setUniqueValues(getNumRecords());
                    break;
                case 0:
                    throw new UnsupportedOperationException("Attribute " + attr.getName() + " not found in Join");
            }
        }
        Condition firstCond = conditionList.get(0);
        setRequestsSortOn(firstCond.getFirstAttribute());
        setRequestsSortOnRight(firstCond.getSecondAttribute());
        for (Condition cond : conditionList) {
            if (cond.getFirstAttribute().equals(outerRelation.getResultSortedOn())) {
                setRequestsSortOn(outerRelation.getResultSortedOn());
                if (cond.getSecondAttribute().equals(innerRelation.getResultSortedOn())) {
                    setRequestsSortOnRight(innerRelation.getResultSortedOn());
                }
                break;
            }
        }
        
        return numRecords;
    }

    @Override
    public int annotateSize() throws SyntaxErrorException {
        /*
         * Ignoring indexed join size estimation optimization.
         * For most if not all cases, the joins won't be performed on relations.
         */
        int innerSize = innerRelation.annotateSize();
        int outerSize = outerRelation.annotateSize();
        Integer uniqueValsInner = 0,
                uniqueValsOuter = 0;

        Set<Attribute> leftAttrSet = innerRelation.getAttributes();
        Set<Attribute> rightAttrSet = outerRelation.getAttributes();

        setAttributes(new HashSet<>());
        for (Attribute attr : leftAttrSet) {
            getAttributes().add(new Attribute(attr));
        }
        for (Attribute attr : rightAttrSet) {
            getAttributes().add(new Attribute(attr));
        }

        for (Condition cond : conditionList) {
            cond.attributeBuildPass(outerRelation.getAttributes(), innerRelation.getAttributes());
            if (!cond.getComparator().equals(CompEnum.EQUALS))
                throw new SyntaxErrorException("Join comparator not consistent with equijoin");
        }

        for (Attribute attr : innerRelation.getAttributes()) {
            Integer uv = attr.getUniqueValues();

            if (uv == null) {
                uniqueValsInner = innerRelation.getSize();
                break;
            }

            uniqueValsInner = (uniqueValsInner < uv)
                    ? uv
                    : uniqueValsInner;
        }

        for (Attribute attr : outerRelation.getAttributes()) {
            Integer uv = attr.getUniqueValues();

            if (uv == null) {
                uniqueValsOuter = outerRelation.getSize();

                break;
            }

            uniqueValsOuter = (uniqueValsOuter < uv)
                    ? uv
                    : uniqueValsOuter;
        }

        int uniqueVals = (uniqueValsInner > uniqueValsOuter)
                ? uniqueValsInner
                : uniqueValsOuter;

        setNumRecords((int) Math.ceil(innerRelation.getSize() * outerRelation.getSize() / (double) uniqueVals));
        for (Attribute attr : getAttributes()) {
            int caseCtr = 0;
            Integer innerAttrUnique = null, outerAttrUnique = null;
            for (Attribute attrInner : innerRelation.getAttributes()) {
                if (attrInner.equals(attr)) {
                    caseCtr += 1;
                    innerAttrUnique = attrInner.getUniqueValues();
                    break;
                }
            }
            for (Attribute attrOuter : outerRelation.getAttributes()) {
                if (attrOuter.equals(attr)) {
                    caseCtr += 1;
                    outerAttrUnique = attrOuter.getUniqueValues();
                    break;
                }
            }
            switch (caseCtr) {
                case 2:
                    if (innerAttrUnique != null) {
                        if (outerAttrUnique != null) {
                            if (innerAttrUnique < outerAttrUnique)
                                attr.setUniqueValues(innerAttrUnique);
                            else
                                attr.setUniqueValues(outerAttrUnique);
                        } else {
                            attr.setUniqueValues(innerAttrUnique);
                        }
                    } else
                        attr.setUniqueValues(outerAttrUnique);
                    //purposely drop to case 1 to check for size & null.
                case 1:
                    if (attr.getUniqueValues() == null || getNumRecords() < attr.getUniqueValues())
                        attr.setUniqueValues(getNumRecords());
                    break;
                case 0:
                    throw new UnsupportedOperationException("Attribute " + attr.getName() + " not found in Join");
            }
        }

        return getNumRecords();
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        innerRelation.annotateCost(catalog);
        outerRelation.annotateCost(catalog);
        return 0;
    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        boolean belongsToCluster = true;
        if (commonConditions == null)
            commonConditions = this.conditionList;
        else {
            if (commonConditions.size() == this.conditionList.size()) {
                for (int i = 0; i < commonConditions.size(); ++i)
                    if (!(commonConditions.get(i).equals(this.conditionList.get(i)))) {
                        belongsToCluster = false;
                        break;
                    }
            } else {
                belongsToCluster = false;
            }
        }
        if (belongsToCluster) {
            joinNodeCluster.add(this);
            if (this.outerRelation instanceof JoinNode)
                outerRelation.getJoinNodeCluster(commonConditions, joinNodeCluster);
            if (this.innerRelation instanceof JoinNode)
                innerRelation.getJoinNodeCluster(commonConditions, joinNodeCluster);
        }
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {
        JoinClusterConditions jcd = new JoinClusterConditions(this.conditionList, this.junctionType);
        if (multiClusterMap.get(jcd) == null)
            multiClusterMap.put(jcd, new HashSet<>());

        if (this.outerRelation instanceof JoinNode)
            this.outerRelation.fillMultiClusterMap(multiClusterMap);
        else {
            Set<TreeNode> treeNodeSet = multiClusterMap.get(jcd);
            treeNodeSet.add(this.outerRelation);
        }

        if (this.innerRelation instanceof JoinNode)
            this.innerRelation.fillMultiClusterMap(multiClusterMap);
        else {
            Set<TreeNode> treeNodeSet = multiClusterMap.get(jcd);
            treeNodeSet.add(this.innerRelation);
        }
    }

    public TreeNode getOuterRelation() {
        return outerRelation;
    }

    public TreeNode getInnerRelation() {
        return innerRelation;
    }

    public List<Condition> getConditionList() {
        return conditionList;
    }

    public JunctionEnum getJunctionType() {
        return junctionType;
    }

    public JoinTypeEnum getJoinType() {
        return joinType;
    }

    public void setJoinType(JoinTypeEnum joinType) {
        this.joinType = joinType;
    }

    public boolean innerFitsMemory() {
        return innerFitsMemory;
    }

    public void setFitInnerMemory(boolean fitInnerMemory) {
        this.innerFitsMemory = fitInnerMemory;
    }

    public boolean isPipelineableInner() {
        return pipelineableInner;
    }

    public void setPipelineableInner(boolean pipelineableInner) {
        this.pipelineableInner = pipelineableInner;
    }

    public boolean outerFitsMemory() {
        return outerFitsMemory;
    }

    public void setOuterFitsMemory(boolean outerFitsMemory) {
        this.outerFitsMemory = outerFitsMemory;
    }
}
