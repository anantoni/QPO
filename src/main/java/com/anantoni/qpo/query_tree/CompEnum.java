package com.anantoni.qpo.query_tree;

/**
 * @author efthymiosh
 */
public enum CompEnum {
    EQUALS, GREATER_THAN, LESS_THAN, LESS_OR_EQUAL, GREATER_OR_EQUAL, NOT_EQUAL_TO
}
