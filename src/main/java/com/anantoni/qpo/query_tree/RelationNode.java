/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.catalog.Catalog;
import com.anantoni.qpo.catalog.Relation;
import com.anantoni.qpo.optimizer.JoinClusterConditions;
import com.anantoni.qpo.query_tree.exception.UnknownRelationException;

import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 * @author efthymiosh
 */
public class RelationNode extends TreeNode {
    private final String name;
    private Relation relation;
    private Set<Relation> relations;

    public RelationNode(String name) {
        this.name = name;
    }

    @Override
    public Set<Relation> referenceBuildPass(Catalog catalog, TreeNode father) throws UnknownRelationException {
        if (father != null) {
            this.getParents().add(father);
        }

        assert (name != null);
        relation = catalog.getRelationFromName(name);

        if (relation == null) {
            throw new UnknownRelationException("Unknown Relation " + name);
        }

        relations = new HashSet<>();
        relations.add(relation);
        this.setResultSortedOn(relation.getPrimaryKey().getDomains().get(0));
        return relations;
    }

    @Override
    public void commandLinePrint() {
        printNodeInfo();
    }

    @Override
    public void printNodeInfo() {
        System.out.println("Relation " + name);
    }

    @Override
    public void printDotNotation(PrintWriter pw) {
        super.printDotNotation(pw);
        pw.print("[label=\"" + name);
        if (resultSortedOn != null) {
            pw.print("\\nSorted On: " + resultSortedOn.getName());
        }
        if (requestsSortOn != null) {
            pw.print("\\nReqSort: " + requestsSortOn.getName());
        }
        if (requestsSortOnRight != null) {
            pw.print("\\nReqSortR: " + requestsSortOnRight.getName());
        }
        pw.println("\" shape=doubleoctagon]");
    }

    @Override
    public void selectionSplit() {
    }

    @Override
    public Set<Relation> getRelations() {
        return relations;
    }

    @Override
    public void addListNodeParentsDFS(Class childType, List<TreeNode> parentList) {
    }

    public void selectionPushdown() {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectionPushdownInit() {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectionPushdown(TreeNode selectionNode) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<TreeNode> getExprAsSet() {
        Set<TreeNode> retVal = new HashSet<>();

        return retVal;
    }

    @Override
    public SelectionNode pushDown(SelectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ProjectionNode pushDown(ProjectionNode node) {
        throw new UnsupportedOperationException("Not supported yet.");    // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void changeChild(TreeNode oldChild, TreeNode newChild) {
        System.err.println("Attempting to replace RelationNode \"child\"");
    }

    @Override
    public int annotateSize() {
        setNumRecords(relation.getTupleAmount());
        setAttributes(new HashSet<>());
        getAttributes().addAll(relation.getAttributeList());
        return numRecords = getNumRecords();
    }

    @Override
    public Integer annotateCost(Catalog catalog) {
        return costRead = 0;
    }

    public void addJoinNodeParentsDFS(Map<TreeNode, List<Condition>> parentList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getJoinNodeCluster(List<Condition> commonConditions, List<JoinNode> joinNodeCluster) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fillMultiClusterMap(Map<JoinClusterConditions, Set<TreeNode>> multiClusterMap) {

    }

    public Relation getRelation() {
        return relation;
    }
}