package com.anantoni.qpo.query_tree;

/**
 * @author efthymiosh
 */
public enum JunctionEnum {
    NONE, AND, OR
}


//~ Formatted by Jindent --- http://www.jindent.com
