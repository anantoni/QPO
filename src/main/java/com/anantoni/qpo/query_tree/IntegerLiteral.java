/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.query_tree;

/**
 * @author anantoni
 */
public class IntegerLiteral implements Operand {
    private final String textRepr;

    public IntegerLiteral(String textRepr) {
        this.textRepr = textRepr;
    }

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);

            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public String toString() {
        return textRepr.replaceAll("\"", "\\\"");
    }

    /**
     * @return a numeric value for the literal.
     * If integer, return itself.
     * If string, calculate a value that is supposedly calculated as the
     * value of the String when inserted in any index in the imaginary DBMS that uses
     * this optimizer.
     */
    @Override
    public Integer getAsNumericalValue() {
        try {
            return Integer.parseInt(textRepr);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Override
    public String getAttributeName() {
        return null;
    }

    @Override
    public String getRelationName() {
        return null;
    }
}
