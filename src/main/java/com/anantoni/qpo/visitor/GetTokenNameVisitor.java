/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.visitor;

//~--- non-JDK imports --------------------------------------------------------

import syntaxtree.*;
import visitor.DepthFirstRetVisitor;

/**
 * @author anantoni
 */
public class GetTokenNameVisitor extends DepthFirstRetVisitor<String> {
    @Override
    public String visit(AttrList n) {
        return n.f1.accept(this);
    }

    @Override
    public String visit(AttrName n) {
        return n.f0.tokenImage;
    }

    @Override
    public String visit(Relation n) {
        return n.f0.tokenImage;
    }

    @Override
    public String visit(Attribute n) {

        String retString = "";
        String relation = n.f0.accept(this);

        if (relation != null) {
            retString += relation;
        }

        String attrName = n.f1.accept(this);

        if (attrName != null) {
            if (relation != null) {
                retString += attrName;
            } else {
                retString += attrName;
            }
        }

        return retString;
    }

    @Override
    public String visit(Operand n) {
        return n.f0.choice.accept(this);
    }

    @Override
    public String visit(Comp n) {
        return n.f0.choice.accept(this);
    }

    @Override
    public String visit(NodeToken n) {
        return n.tokenImage;
    }

    @Override
    public String visit(NodeSequence n) {
        String retStr = "";

        for (INode in : n.nodes) {
            retStr += in.accept(this);
        }

        return retStr;
    }

    @Override
    public String visit(NodeListOptional n) {
        String retStr = "";

        for (INode in : n.nodes) {
            retStr += in.accept(this);
        }

        return retStr;
    }

    @Override
    public String visit(NodeList n) {
        String retStr = "";

        for (INode in : n.nodes) {
            retStr += in.accept(this);
        }

        return retStr;
    }
}