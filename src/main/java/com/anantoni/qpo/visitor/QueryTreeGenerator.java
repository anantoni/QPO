/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.visitor;

//~--- non-JDK imports --------------------------------------------------------

import com.anantoni.qpo.query_tree.*;
import syntaxtree.*;
import visitor.DepthFirstRetArguVisitor;

import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 */
public class QueryTreeGenerator extends DepthFirstRetArguVisitor<TreeNode, String> {
    private final GetTokenNameVisitor getTokenNameVisitor;
    private final GetTokenNamesListVisitor getTokenNamesListVisitor;

    public QueryTreeGenerator() {
        getTokenNameVisitor = new GetTokenNameVisitor();
        getTokenNamesListVisitor = new GetTokenNamesListVisitor(getTokenNameVisitor);
    }

    @Override
    public TreeNode visit(final SelectionExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        List<String> condition = n.f2.accept(getTokenNamesListVisitor);
        List<String> conditionList = n.f3.accept(getTokenNamesListVisitor);

        condition.addAll(conditionList);

        n.f4.accept(this, argu);
        n.f5.accept(this, argu);

        TreeNode expr = n.f6.accept(this, "return");
        if (expr == null)
            System.err.println("Error in visitor - NULL expression in selection");
        return new SelectionNode(condition, expr);
    }

    @Override
    public TreeNode visit(final ProjectionExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        List<String> attribute = n.f2.accept(getTokenNamesListVisitor);
        List<String> attributeList = n.f3.accept(getTokenNamesListVisitor);

        attribute.addAll(attributeList);

        n.f4.accept(this, argu);
        n.f5.accept(this, argu);

        TreeNode expr = n.f6.accept(this, "return");

        return new ProjectionNode(attribute, expr);
    }

    @Override
    public TreeNode visit(final JoinExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        List<String> condition = n.f2.accept(getTokenNamesListVisitor);
        List<String> conditionList = n.f3.accept(getTokenNamesListVisitor);

        condition.addAll(conditionList);

        n.f4.accept(this, argu);
        n.f5.accept(this, argu);

        TreeNode expr1 = n.f6.accept(this, "return");

        n.f7.accept(this, argu);
        n.f8.accept(this, argu);

        TreeNode expr2 = n.f9.accept(this, "return");

        n.f10.accept(this, argu);

        return new JoinNode(condition, expr1, expr2);
    }

    @Override
    public TreeNode visit(final UnionExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        TreeNode expr1 = n.f2.accept(this, argu);

        n.f3.accept(this, argu);
        n.f4.accept(this, argu);

        TreeNode expr2 = n.f5.accept(this, argu);

        n.f6.accept(this, argu);

        return new UnionNode(expr1, expr2);
    }

    @Override
    public TreeNode visit(final InterExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        TreeNode expr1 = n.f2.accept(this, argu);

        n.f3.accept(this, argu);
        n.f4.accept(this, argu);

        TreeNode expr2 = n.f5.accept(this, argu);

        n.f6.accept(this, argu);

        return new InterNode(expr1, expr2);
    }

    @Override
    public TreeNode visit(final DiffExpr n, final String argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);

        TreeNode expr1 = n.f2.accept(this, argu);

        n.f3.accept(this, argu);
        n.f4.accept(this, argu);

        TreeNode expr2 = n.f5.accept(this, argu);

        n.f6.accept(this, argu);

        return new DiffNode(expr1, expr2);
    }

    @Override
    public TreeNode visit(final Relation n, final String argu) {
        n.f0.accept(this, argu);

        TreeNode relationNode = null;
        if (argu != null) {
            relationNode = new RelationNode(n.f0.tokenImage);
        }

        return relationNode;
    }

    @Override
    public TreeNode visit(Expr n, String argu) {
        TreeNode expr = n.f0.accept(this, argu);

        return expr;
    }

    @Override
    public TreeNode visit(Query n, String argu) {
        TreeNode query = n.f0.accept(this, argu);

        return query;
    }
}
