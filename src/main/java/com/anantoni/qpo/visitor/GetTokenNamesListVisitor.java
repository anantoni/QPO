/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
 */
package com.anantoni.qpo.visitor;

//~--- non-JDK imports --------------------------------------------------------

import syntaxtree.*;
import visitor.DepthFirstRetVisitor;

import java.util.ArrayList;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author anantoni
 */
public class GetTokenNamesListVisitor extends DepthFirstRetVisitor<List<String>> {
    GetTokenNameVisitor getNameVisitor;

    public GetTokenNamesListVisitor(GetTokenNameVisitor visitor) {
        getNameVisitor = visitor;
    }

    @Override
    public List<String> visit(Attribute n) {
        List<String> retList = new ArrayList<>();

        retList.add(n.accept(getNameVisitor));
        return retList;
    }

    @Override
    public List<String> visit(AttrList n) {
        List<String> retList = n.f0.accept(this);

        retList.addAll(n.f1.accept(this));

        return retList;
    }

    @Override
    public List<String> visit(Condition n) {
        List<String> retList = new ArrayList();

        retList.add(n.f0.accept(getNameVisitor));
        retList.add(n.f1.accept(getNameVisitor));
        retList.add(n.f2.accept(getNameVisitor));

        return retList;
    }

    @Override
    public List<String> visit(ConditionList n) {
        return n.f0.choice.accept(this);
    }

    @Override
    public List<String> visit(NodeToken n) {
        List<String> retList = new ArrayList<>();

        retList.add(n.accept(getNameVisitor));

        return retList;
    }

    @Override
    public List<String> visit(NodeSequence n) {
        List<String> retList = new ArrayList<>();

        for (INode in : n.nodes) {
            retList.addAll(in.accept(this));
        }

        return retList;
    }

    @Override
    public List<String> visit(NodeListOptional n) {

        List<String> retList = new ArrayList<>();

        for (INode in : n.nodes) {
            retList.addAll(in.accept(this));
        }

        return retList;
    }

    @Override
    public List<String> visit(NodeList n) {

        List<String> retList = new ArrayList<>();

        for (INode in : n.nodes) {
            retList.add(in.accept(getNameVisitor));
        }

        return retList;
    }
}