/* Generated by JTB 1.4.4 */
package visitor;

//~--- non-JDK imports --------------------------------------------------------

import syntaxtree.*;

import java.util.Iterator;

//~--- JDK imports ------------------------------------------------------------

public class DepthFirstVoidArguVisitor<A> implements IVoidArguVisitor<A> {
    public void visit(final NodeChoice n, final A argu) {
        n.choice.accept(this, argu);

        return;
    }

    public void visit(final NodeList n, final A argu) {
        for (final Iterator<INode> e = n.elements(); e.hasNext(); ) {
            e.next().accept(this, argu);
        }

        return;
    }

    public void visit(final NodeListOptional n, final A argu) {
        if (n.present()) {
            for (final Iterator<INode> e = n.elements(); e.hasNext(); ) {
                e.next().accept(this, argu);
            }

            return;
        } else {
            return;
        }
    }

    public void visit(final NodeOptional n, final A argu) {
        if (n.present()) {
            n.node.accept(this, argu);

            return;
        } else {
            return;
        }
    }

    public void visit(final NodeSequence n, final A argu) {
        for (final Iterator<INode> e = n.elements(); e.hasNext(); ) {
            e.next().accept(this, argu);
        }

        return;
    }

    public void visit(final NodeToken n, @SuppressWarnings("unused") final A argu) {
        @SuppressWarnings("unused") final String tkIm = n.tokenImage;

        return;
    }

    public void visit(final Query n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
    }

    public void visit(final Expr n, final A argu) {
        n.f0.accept(this, argu);
    }

    public void visit(final SelectionExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
        n.f7.accept(this, argu);
    }

    public void visit(final ProjectionExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
        n.f7.accept(this, argu);
    }

    public void visit(final JoinExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
        n.f7.accept(this, argu);
        n.f8.accept(this, argu);
        n.f9.accept(this, argu);
        n.f10.accept(this, argu);
    }

    public void visit(final UnionExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
    }

    public void visit(final InterExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
    }

    public void visit(final DiffExpr n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
    }

    public void visit(final ConditionList n, final A argu) {
        n.f0.accept(this, argu);
    }

    public void visit(final Condition n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
        n.f2.accept(this, argu);
    }

    public void visit(final Comp n, final A argu) {
        n.f0.accept(this, argu);
    }

    public void visit(final Operand n, final A argu) {
        n.f0.accept(this, argu);
    }

    public void visit(final AttrList n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
    }

    public void visit(final Attribute n, final A argu) {
        n.f0.accept(this, argu);
        n.f1.accept(this, argu);
    }

    public void visit(final Relation n, final A argu) {
        n.f0.accept(this, argu);
    }

    public void visit(final AttrName n, final A argu) {
        n.f0.accept(this, argu);
    }
}
