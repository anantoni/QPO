lParser Generation and Visitor Generation instructions

1) You will find grammar.jj under src/main/java/com/anantoni/qpo/parser
2) Execute "javacc grammar.jj". Package declarations are already set.
3) Execute "jtb grammar.jj" to produce the syntaxtree and visitor directories.
4) Move syntaxtree and visitor under src/main/java
5) Execute "javacc jtb.out.jj" to generate the visiting parser (remember to add "package com.anantoni.qpo.parser;" to jtb.out.jj before running javacc"
6) Call the RelAlgParser from com.anantoni.qpo.optimizer/Driver.java
